<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\Response;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\ContactForm;

class SiteController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [];
    }

    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex()
    {             
        return $this->render('index');
    }

    /**
     * Login action.
     *
     * @return Response|string
     */
    public function actionLogin()
    {      
        $session = Yii::$app->session; 
        if($session['logado']){
            return $this->goHome();
        }            
        
        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post())) {            
            $connection = Yii::$app->getDb("db2");
            $command = $connection->createCommand("SELECT "                    
                    . "                                     NM_USUARIO "
                    . "                                 FROM "
                    . "                                     BD_SICA.USUARIOS"
                    . "                                 WHERE "
                    . "                                     LOGIN = '".$model->username."'"
                    . "                                 AND"
                    . "                                     SENHA_INTERNET ='".$model->password."'"
                    . "                                 AND"
                    . "                                     ATIVO = 1");                        
            $result = $command->queryAll();
            if($result){                
                $session['logado'] = "true";                
                return $this->goHome();
            }            

        }
        return $this->render('login', [
            'model' => $model,
        ]);
    }

    /**
     * Logout action.
     *
     * @return Response
     */
    public function actionLogout()
    {                
        $session = Yii::$app->session; 
        $session['logado'] = null;     
        return $this->redirect(['site/login']);
    }

    /**
     * Displays contact page.
     *
     * @return Response|string
     */
    public function actionContact()
    {
        $model = new ContactForm();
        if ($model->load(Yii::$app->request->post()) && $model->contact(Yii::$app->params['adminEmail'])) {
            Yii::$app->session->setFlash('contactFormSubmitted');

            return $this->refresh();
        }
        return $this->render('contact', [
            'model' => $model,
        ]);
    }

    /**
     * Displays about page.
     *
     * @return string
     */
    public function actionAbout()
    {
        return $this->render('about');
    }
}
