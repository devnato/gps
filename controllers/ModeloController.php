<?php

namespace app\controllers;

use Yii;
use app\models\Modelo;
use app\models\ModeloSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use \app\models\Componente;
use \app\models\ModeloComponente;

/**
 * ModeloController implements the CRUD actions for Modelo model.
 */
class ModeloController extends Controller
{
    /**
     * @inheritdoc
     */
    
    var $componentes = [];
    
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }
    
    public function beforeAction($action)
    {       
        $session = Yii::$app->session;                 
        if(!$session['logado']){
            return $this->redirect(['site/login']);
        }
        return true;
    }

    /**
     * Lists all Modelo models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new ModeloSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $session = Yii::$app->session;
        $session['componentes'] = null;        
        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Modelo model.
     * @param double $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Modelo model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {        
        $model = new Modelo();        
        $modelComponente = new Componente();

        $searchModelComponente = new \app\models\ComponenteSearch();
        $dataProviderComponente = $searchModelComponente->search(Yii::$app->request->queryParams);        
        $dataProviderComponente->pagination = ['pageSize' => 7];
        $dataProviderComponente->sort->sortParam = false;
        
        $session = Yii::$app->session; 
        $arraycomponentes = $session['componentes']; 
        $providerComponente = new \yii\data\ArrayDataProvider([
            'allModels' => $arraycomponentes,
            'sort' => [
                'attributes' => ['CODIGO'],
            ],
        ]);                
        
        if ($model->load(Yii::$app->request->post()) && $model->save()) {                                    
            $this->salvarComponentes($arraycomponentes, $model->ID_MODELO);            
            $session['componentes'] = null;              
            return $this->redirect(['view', 'id' => $model->ID_MODELO]);
        } else {                        
            return $this->render('create', [                
                'model' => $model,
                'searchModelComponente'     => $searchModelComponente,
                'dataProviderComponente'    => $dataProviderComponente,   
                'modelComponente'           => $modelComponente, 
                'providerComponente'        => $providerComponente,
              ]);
        }
    }
    
    function salvarComponentes($arraycomponentes, $idModelo){
        $this->excluirModeloComponentes($idModelo); 
        if(count($arraycomponentes)>0){                                            
            foreach ($arraycomponentes as $val){                                                                                                                
                if(in_array($val,$arraycomponentes)){
                    $val['ID_MODELO'] = $idModelo;
                    $modeloComponente = $this->persistirDados($val);                                    
                    array_splice($arraycomponentes, array_search($val,$arraycomponentes),1);                                                  
                }                
                $arraycomponentes = array_values($this->salvarSubComponentes($arraycomponentes, $modeloComponente));                
            }                 
        }
    }
    
    function excluirModeloComponentes($idModelo){        
        ModeloComponente::deleteAll(['ID_MODELO'=>$idModelo]);        
    }
    
    function salvarSubComponentes($arraycomponentes, $modelo){        
        foreach ($arraycomponentes as $val){ 
            if ($val['ID_MODELO_COMPONENTE_PARENT'] === $modelo->ID_COMPONENTE){                                                                                                                    
                if(in_array($val,$arraycomponentes)){                    
                    $val['ID_MODELO_COMPONENTE_PARENT'] =   $modelo->ID_MODELO_COMPONENTE;
                    $val['ID_MODELO']                   =   $modelo->ID_MODELO;                                                 
                    $modeloComponente = $this->persistirDados($val);                                    
                    array_splice($arraycomponentes, array_search($val,$arraycomponentes),1);                                                  
                }   
                $arraycomponentes = array_values($this->salvarSubComponentes($arraycomponentes, $modeloComponente));                
            }
        }
        return array_values($arraycomponentes);
    }
    
    function persistirDados($val){
        $modeloComponente = new ModeloComponente();
        $modeloComponente->ID_COMPONENTE =                  $val['ID_COMPONENTE'];
        $modeloComponente->CODIGO =                         $val['CODIGO']."";
        $modeloComponente->ID_MODELO_COMPONENTE_PARENT =    $val['ID_MODELO_COMPONENTE_PARENT'];
        $modeloComponente->ID_MODELO =                      $val['ID_MODELO'];        
        $modeloComponente->save(); 
        return $modeloComponente;
    }
    
    function imprimeArray($array){
        echo "imprimindo...<br>";
        foreach ($array as $key => $val){ 
            echo $key." - ".$val['CODIGO']." <BR> ";
        }
        echo "<br>";
    }

    /**
     * Updates an existing Modelo model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param double $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        $modelComponente = new Componente();
        
        $searchModelComponente = new \app\models\ComponenteSearch();
        $dataProviderComponente = $searchModelComponente->search(Yii::$app->request->queryParams);        
        $dataProviderComponente->pagination = ['pageSize' => 7];        
        $session = Yii::$app->session; 
        if(!isset($session['componentes'])){
            $session['componentes'] = $model->mODELOCOMPONENTEs;
        }
        $arrayModeloComponentes = $session['componentes'];         
        $this->array_sort_by_column($arrayModeloComponentes,"CODIGO"); 
        $providerComponente = new \yii\data\ArrayDataProvider([
            'allModels' => $arrayModeloComponentes,
            'sort' => [
                'attributes' => ['CODIGO'],
            ],
        ]);         
        
        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            $this->salvarComponentes($arrayModeloComponentes, $model->ID_MODELO);            
            $session['componentes'] = null; 
            return $this->redirect(['view', 'id' => $model->ID_MODELO]);
        } else {
            return $this->render('update', [
                'model' => $model,
                'searchModelComponente'     => $searchModelComponente,
                'dataProviderComponente'    => $dataProviderComponente,   
                'modelComponente'           => $modelComponente, 
                'providerComponente'        => $providerComponente,
            ]);
        }
    }

    /**
     * Deletes an existing Modelo model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param double $id
     * @return mixed
     */
    public function actionDelete($id)
    {        
        $this->excluirModeloComponentes($id);
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Modelo model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param double $id
     * @return Modelo the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Modelo::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
    
    public function actionAdicionaComponente()
    {        
        if (Yii::$app->request->isAjax) {                        
            $session = Yii::$app->session;                                    
            $componente = $this->getComponente(Yii::$app->request->post());               
            $this->adicionaComponenteNaSessao($componente, $session);
        }
    }
    
    public function actionRemoveComponente()
    {        
        if (Yii::$app->request->isAjax) {                                    
            $session = Yii::$app->session;
            $ID_COMPONENTE = explode(":", Yii::$app->request->post()['ID_COMPONENTE'])[0];  
            $arraycomponentes = $session['componentes'];                            
            $found_key = array_search($ID_COMPONENTE, array_column($arraycomponentes, 'ID_COMPONENTE'));                        
            $idPai = $arraycomponentes[$found_key]['ID_MODELO_COMPONENTE_PARENT'];
            $code = $arraycomponentes[$found_key]['CODIGO'];
            unset($arraycomponentes[$found_key]);               
            $newArrayComponentes = $this->removeFilhosComponente($ID_COMPONENTE, array_values($arraycomponentes));
            //rearrange códigos
            if(strpos($code, '.')){
                $code = explode('.', $code)[substr_count($code, '.')];
            }
            $this->rearrangeCode($newArrayComponentes, $idPai, $code);
            $session['componentes'] = $newArrayComponentes;                                            
        }
    }
    
    function rearrangeCode($arraycomponentes, $idPai = "", $codeExcluido = 1){
        
        if($idPai){ //tem um componente pai
            foreach ($arraycomponentes as $key => $val){ 
                if ($val['ID_MODELO_COMPONENTE_PARENT'] === $idPai){
                    //primeiro pega o último dígito
                    $code = explode('.', $val['CODIGO'])[substr_count($val['CODIGO'], '.')];   
                    if($code>$codeExcluido){
                        $arraycomponentes[$key]['CODIGO'] = $this->retornaPrefixCode(explode('.', $val['CODIGO'])).".".($code-1); 
                        //AQUI DEVERÁ ATUALIZAR OS CÓDIGOS DOS FILHOS
                        $this->atualizaPrefixo($arraycomponentes, $arraycomponentes[$key]['CODIGO'], $arraycomponentes[$key]['ID_COMPONENTE']);
                    }                    
                }
            } 
        }else{ // não tem um componente pai            
            foreach ($arraycomponentes as $key => $val){ 
                if ($val['ID_MODELO_COMPONENTE_PARENT'] == "" || $val['ID_MODELO_COMPONENTE_PARENT'] == null){
                    //primeiro pega o último dígito
                    $code = $val['CODIGO'];   
                    if($code>$codeExcluido){
                        $arraycomponentes[$key]['CODIGO'] = $code-1;
                        //AQUI DEVERÁ ATUALIZAR OS CÓDIGOS DOS FILHOS
                        $this->atualizaPrefixo($arraycomponentes, $arraycomponentes[$key]['CODIGO'], $arraycomponentes[$key]['ID_COMPONENTE']);
                    }                    
                }
            }
        }
    }
    
    function atualizaPrefixo($arraycomponentes, $prefixo, $idPai){
        foreach ($arraycomponentes as $key => $val){ 
            if ($val['ID_MODELO_COMPONENTE_PARENT'] === $idPai){
                //primeiro pega o último dígito
                $code = explode('.', $val['CODIGO'])[substr_count($val['CODIGO'], '.')];                                                            
                $arraycomponentes[$key]['CODIGO'] = $prefixo.".".$code; 
                //AQUI DEVERÁ ATUALIZAR OS CÓDIGOS DOS FILHOS
                $this->atualizaPrefixo($arraycomponentes, $arraycomponentes[$key]['CODIGO'], $arraycomponentes[$key]['ID_COMPONENTE']);
            }
        }
    }
        
    function retornaPrefixCode($array){
        $retorno = "";
        for($x = 0; $x <= (count($array)-2); $x++){
            $retorno .= $array[$x].".";
        }
        return substr($retorno, 0, -1);
    }


    function removeFilhosComponente($id, $arraycomponentes){                                                   
        foreach ($arraycomponentes as $key => $val){                        
            if ($val['ID_MODELO_COMPONENTE_PARENT'] === $id){                
                $id_para_sub = $val['ID_COMPONENTE'];
                array_splice($arraycomponentes, array_search($val,$arraycomponentes),1);                
                $arraycomponentes = array_values($this->removeFilhosComponente($id_para_sub, array_values($arraycomponentes)));
            }
        }             
        return array_values($arraycomponentes);
    }    
    
    function getComponente($data){
        $ID_COMPONENTE = explode(":", $data['ID_COMPONENTE'])[0];                  
        $TITULO = explode(":", $data['TITULO'])[0];             
        $IDPARENT = explode(":", $data['ID_PARENT'])[0];       
        $CODEPARENT = explode(":", $data['CODE_PARENT'])[0];
        $modeloComponente = new \app\models\ModeloComponente();        
        $modeloComponente->ID_COMPONENTE = $ID_COMPONENTE;        
        $modeloComponente->TITULO = $TITULO;                     
        $modeloComponente->CODIGO = $this->getCodigoComponente($IDPARENT,$CODEPARENT);                
        $modeloComponente->ID_MODELO_COMPONENTE_PARENT = ($IDPARENT != "" && $IDPARENT != NULL)? $IDPARENT : "";
        return $modeloComponente;
    } 
    
    function getCodigoComponente($id,$code){        
        $session = Yii::$app->session; 
        $arraycomponentes = $session['componentes'];        
        $count = 1;                   
        if($id != "" && $id != NULL){                        
            foreach ($arraycomponentes as $key => $val){ 
                if ($val['ID_MODELO_COMPONENTE_PARENT'] === $id){
                    $count++;
                }
            }
            $count = $code.'.'.$count;
        }else{                                
            $count = $this->getCodigoRaiz($arraycomponentes);
        }      
        return $count;
    }
    
    function getCodigoRaiz($arraycomponentes){
        $count = 1;                
        if($arraycomponentes){
            foreach($arraycomponentes as $key => $val){                 
                if ($val['ID_MODELO_COMPONENTE_PARENT'] === "" || $val['ID_MODELO_COMPONENTE_PARENT'] === NULL){
                    $count++;
                }
            }
        } 
        return $count;
    }           
    
    function adicionaComponenteNaSessao($componente, $session){                 
        if (!isset($session['componentes']) || count($session['componentes'])==0){                                                
            $session['componentes'] = array($componente);
        }else{
            if(!$this->existeNoArray($session['componentes'], $componente->ID_COMPONENTE)){                
                $arraycomponentes = $session['componentes'];
                $arraycomponentes[] = $componente;                
                $this->array_sort_by_column($arraycomponentes, 'CODIGO');
                $session['componentes'] = $arraycomponentes;                                
            }
        }         
    }
    
    function existeNoArray($arraycomponentes, $id) {                        
        foreach($arraycomponentes as $val){                 
            if ($val['ID_COMPONENTE'] === $id){                
                return true;
            }
        }
        return false;
    }
    
    function array_sort_by_column(&$arr, $col, $dir = SORT_ASC) {
        $sort_col = array();
        foreach ($arr as $key=> $row) {            
            $sort_col[$key] = $row[$col];
        }        
        array_multisort($sort_col, $dir, $arr);        
    } 

}
