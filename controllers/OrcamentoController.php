<?php

namespace app\controllers;

use Yii;
use app\models\Orcamento;
use app\models\OrcamentoSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * OrcamentoController implements the CRUD actions for Orcamento model.
 */
class OrcamentoController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    public function beforeAction($action)
    {       
        $session = Yii::$app->session;                 
        if(!$session['logado']){
            return $this->redirect(['site/login']);
        }
        return true;
    }
    
    /**
     * Lists all Orcamento models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new OrcamentoSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Orcamento model.
     * @param string $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Orcamento model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
//    public function actionCreate($id)
//    {
//        $model = new Orcamento();
//
//        if ($model->load(Yii::$app->request->post()) && $model->save()) {
//            return $this->redirect(['view', 'id' => $model->ID_ORCAMENTO]);
//        } else {
//            return $this->render('create', [
//                'model'     => $model,
//                'idProjeto' => $id,
//            ]);
//        }
//    }  
    
    public function actionCreate($id)
    {                                        
        $array_providers = $this->prepara($id);        
        if ($array_providers['model']->load(Yii::$app->request->post())) {                        
            $array_providers['model']->VALOR_TOTAL = $array_providers['model']->VALOR_UNITARIO * $array_providers['model']->QUANTIDADE;
            if($array_providers['model']->save()){
            }else{          
            }            
            $model = new Orcamento();
            $model->ID_PROJETO = $id;
            $array_providers['model'] = $model;            
            return $this->render('create', $array_providers);
        } else {                                    
            return $this->render('create', $array_providers);
        }
    }

    /**
     * Updates an existing Orcamento model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param string $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->ID_ORCAMENTO]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing Orcamento model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param string $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Orcamento model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param string $id
     * @return Orcamento the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Orcamento::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
    
    function prepara($id){
        $model          = new Orcamento();        
        $modelCategoria = new \app\models\Categoria();    
        $modelGasto     = new \app\models\Gasto();    
        $model->ID_PROJETO = $id;     
        $model->QUANTIDADE = 1;
        $dataProviderGasto      = $this->getDataProviderGasto();
        $dataProviderCategoria  = $this->getDataProviderCategoria();  
        
        $searchModel = new OrcamentoSearch(['ID_PROJETO' => $id]);                        
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $dataProvider->pagination = ['pageSize' => 10];
        $dataProvider->sort->sortParam = false;
        
        return [                
                'model'                     => $model,                
                'searchModelGasto'          => new \app\models\GastoSearch(),
                'searchModelCategoria'      => new \app\models\CategoriaSearch(),
                'dataProviderGasto'         => $dataProviderGasto, 
                'dataProviderCategoria'     => $dataProviderCategoria, 
                'idProjeto'                 => $id,
                'modelGasto'                => $modelGasto,
                'modelCategoria'            => $modelCategoria,
                'dataProvider'              => $dataProvider,
              ];        
    }
    
    function getDataProviderGasto(){
        $searchModelGasto = new \app\models\gastoSearch();
        $dataProviderGasto = $searchModelGasto->search(Yii::$app->request->queryParams);        
        $dataProviderGasto->pagination = ['pageSize' => 7];
        $dataProviderGasto->sort->sortParam = false;
        return $dataProviderGasto;
    }
    
    function getDataProviderCategoria(){
        $searchModelCategoria = new \app\models\CategoriaSearch();
        $dataProviderCategoria = $searchModelCategoria->search(Yii::$app->request->queryParams);        
        $dataProviderCategoria->pagination = ['pageSize' => 7];
        $dataProviderCategoria->sort->sortParam = false;
        return $dataProviderCategoria;
    }
}
