<?php

namespace app\controllers;

use Yii;
use app\models\Projeto;
use app\models\ProjetoSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use app\models\Modelo;
use app\models\ProjetoComponente;
use yii\db\Expression;


/**
 * ProjetoController implements the CRUD actions for Projeto model.
 */
class ProjetoController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }
    
    public function beforeAction($action)
    {       
        $session = Yii::$app->session;                 
        if(!$session['logado']){
            return $this->redirect(['site/login']);
        }
        return true;
    }

    /**
     * Lists all Projeto models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new ProjetoSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Projeto model.
     * @param double $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Projeto model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Projeto();
        if($model->load(Yii::$app->request->post())){            
            //$model->DT_INICIO = new Expression("to_date('" . $model->DT_INICIO . "','yyyy-mm-dd')");
            //$model->DT_INICIO   = 'to_date("'.$model->DT_INICIO.'", "yyyy-mm-dd")';
            //$model->DT_TERMINO  = "to_date('".$model->DT_TERMINO."', 'yyyy-mm-dd')";
            if ($model->save()) {
                $componentes = [];
                $modelo = Modelo::findOne(['ID_MODELO'=>$model->ID_MODELO]);   
                $modeloComponentes = $this->array_sort_by_column($modelo->mODELOCOMPONENTEs, "CODIGO");
                foreach($modeloComponentes as $modeloComponente){                                                                              
                    $projetoComponente = new ProjetoComponente();
                    $projetoComponente->COMPONENTE_ID_COMPONENTE = $modeloComponente->ID_COMPONENTE;
                    $componente = \app\models\Componente::findOne(['ID_COMPONENTE'=>$modeloComponente->ID_COMPONENTE]);                        
                    $projetoComponente->TITULO = $componente->TITULO;
                    $projetoComponente->CODIGO = $modeloComponente->CODIGO;
                    $projetoComponente->PROJETO_ID_PROJETO = $model->ID_PROJETO;
                    $projetoComponente->save();                
                    $componentes[] = $projetoComponente;
                }                          
                return $this->render('componentes', [
                    'model' => $model,
                    'componentes' => $componentes,
                ]);
            }
        }else {
            return $this->render('create', [
                'model' => $model,                
            ]);
        }
    }   

    /**
     * Updates an existing Projeto model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param double $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->ID_PROJETO]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing Projeto model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param double $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Projeto model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param double $id
     * @return Projeto the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Projeto::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
    
    public function actionAdicionaComponente()
    {        
        if (Yii::$app->request->isAjax) {                                                          
            $data = Yii::$app->request->post();                          
            foreach ($data['myData'] as $valor){
                reset($valor);
                $key = key($valor);    
                $projetoComponente = ProjetoComponente::findOne(['ID_PROJETO_COMPONENTE' => $key]);
                $projetoComponente->DESCRICAO = $valor[$key];
                $projetoComponente->update();
            }
        }
    }
    
    public function actionGerarPdf($id)
    {                        
        require_once '..\vendor\autoload.php';                
        $mpdf = new \Mpdf\Mpdf();     
        $model = $this->findModel($id);
        $projetoComponente = ProjetoComponente::findAll(['PROJETO_ID_PROJETO' => $id]);
        $projetoComponente = $this->array_sort_by_column($projetoComponente, "CODIGO");        
        $mpdf->WriteHTML($this->getCapa($projetoComponente, $model));
        $mpdf->WriteHTML('<pagebreak suppress="off" />');
        $mpdf->WriteHTML('<pagebreak suppress="off" />');                   
        $mpdf->WriteHTML($this->getContraCapa($projetoComponente, $model));
        $mpdf->WriteHTML('<pagebreak suppress="off" />');
        $mpdf->WriteHTML('<pagebreak suppress="off" />');                
        $mpdf->WriteHTML($this->getSumario($projetoComponente));
        $mpdf->WriteHTML('<pagebreak suppress="off" />');
        $mpdf->setFooter('{PAGENO}');  
        $cont = 1;
        $qtdLinhas = 0;        
        foreach($projetoComponente as $componente){                                                            
            $html = "";                    
            $qtdLinhas = $this->quebraLinha($componente->CODIGO, $cont, $mpdf, $qtdLinhas);            
            echo $componente->TITULO." - ".$qtdLinhas."<br>";                        
            $negritoIni = "";$negritoFim = "";$brs = "";
            if(!strpos($componente->CODIGO, '.')){
                $qtdLinhas = $this->getNumeroLinhas($componente->DESCRICAO);
                $negritoIni = "<b>";
                $negritoFim = "</b>";
                $brs = "<br>";                
            }else{
                $qtdLinhas = $this->getNumeroLinhas($componente->DESCRICAO)+$qtdLinhas;
                $brs = "";
            }
            $html .= $brs."
                <p style='font-size: 22px;'>".
                            $negritoIni.$componente->CODIGO." ".$componente->TITULO.$negritoFim.
                            "</p>".$brs;
            $html .= $componente->DESCRICAO;
            $mpdf->WriteHTML($html); 
            $cont++;
        }
        $projetoAtividades = \app\models\ProjetoAtividade::findAll(['ID_PROJETO' => $id]);        
        $mpdf->WriteHTML('<pagebreak suppress="off" />');
        $mpdf->WriteHTML($this->getAtividades($projetoAtividades));
        $mpdf->WriteHTML('<pagebreak suppress="off" />');
        $mpdf->WriteHTML($this->getResponsabilidades($projetoAtividades));        
        $mpdf->setFooter('{PAGENO}'); 
        $mpdf->Output();        
    }
    
    public function actionEditarComponente($id)
    {
        $model = $this->findModel($id);
                
        $componentes = ProjetoComponente::findAll(['PROJETO_ID_PROJETO'=>$id]);                        
        $componentes = $this->array_sort_by_column($componentes, "CODIGO");
        return $this->render('componentes', [
            'model' => $model,
            'componentes' => $componentes,
        ]);        
    } 
    
    function quebraLinha($codigo, $pagina, $mpdf, $linhas){
        if(!strpos($codigo, '.')){            
            $mpdf->WriteHTML(($pagina > 1)?'<pagebreak suppress="off" />':"");            
            $linhas = 0;
        }else{
            if($linhas>20){
                $mpdf->WriteHTML(($pagina > 1)?'<pagebreak suppress="off" />':"");
                $linhas = 0;
            }
        }
        return $linhas;
    }
    
    function getNumeroLinhas($string){
        $descricao = str_replace('&nbsp; ', '', $string);
        $descricao = str_replace('&nbsp;', '', $descricao);
        $qtdLinhas = floor(strlen(strip_tags($descricao))/70);
        $qtdBr = substr_count($string, '<br>');
        $qtdP = substr_count($string, '<p>');
        $qtdLinhas +=($qtdBr > $qtdP)? $qtdBr : $qtdP;
        $qtdLinhas += substr_count($string, '<li>');
        return $qtdLinhas;
    }
    
    function getCapa($projetoComponente, $model){        
        $html = '<style> 
                    .test{                                                 
                        background:url(http://localhost'.\yii\helpers\Url::to('@web/css/dot.jpg').') repeat-x;                                                
                    }
                 </style>';
        
        $html .= "                
                <br><br><br><br>                
                <table width='100%'>
                    <tr>
                      <td class='contentDetails'>
                       <th align='left'></th>
                       <th align='center'><p style='font-size: 22px;'><b>SÉCULO MANAUS </b></p></th>
                       <th align='right'></th>
                      </td>
                    </tr>
                </table>                                
                <br><br><br><br><br>
                <br>   
                <table width='100%'>
                    <tr>
                      <td class='contentDetails'>
                       <td align='left'></td>
                       <td align='center'><p style='font-size: 18px;'>".$model->RESPONSAVEL." </p></td>
                       <td align='right'></td>
                      </td>
                    </tr>
                </table>                
                <br><br><br><br><br>
                <br><br><br><br><br> 
                <br><br><br>
                <table width='100%'>
                    <tr>
                      <td class='contentDetails'>
                       <th align='left'></th>
                       <th align='center'><p style='font-size: 24px;'><b>".$model->TITULO." </b></p></th>
                       <th align='right'></th>
                      </td>
                    </tr>
                </table>                
                <br><br><br><br><br>
                <br><br><br><br><br>
                <br><br><br><br><br>
                <br><br><br>
                <table width='100%'>
                    <tr>
                      <td class='contentDetails'>
                       <th align='left'></th>
                       <th align='center'><p style='font-size: 20px;'><b>Manaus, AM </b></p></th>                       
                       <th align='right'></th>
                      </td>
                    </tr>
                    <tr>
                      <td class='contentDetails'>
                       <th align='left'></th>                       
                       <th align='center'><p style='font-size: 20px;'><b>".date("Y")." </b></p></th>
                       <th align='right'></th>
                      </td>
                    </tr>
                </table>";        
        return $html;
    }
    
    function getContraCapa($projetoComponente, $model){        
        
        $html = "                
                <br><br><br>                
                <table width='100%'>
                    <tr>
                      <td class='contentDetails'>
                       <td align='left'></td>
                       <td align='center'><p style='font-size: 22px;'>".$model->RESPONSAVEL."</p></td>
                       <td align='right'></td>
                      </td>
                    </tr>
                </table>                                
                <br><br><br><br><br><br>
                <br>   
                <table width='100%'>
                    <tr>
                      <td class='contentDetails'>
                       <td align='left'></td>
                       <td align='center'><p style='font-size: 18px;'></p></td>
                       <td align='right'></td>
                      </td>
                    </tr>
                </table>                
                <br><br><br><br><br>
                <br><br><br><br><br> 
                <br><br><br>
                <table width='100%'>
                    <tr>
                      <td class='contentDetails'>
                       <th align='left'></th>
                       <th align='center'><b><span style='font-size: 24px;'>".$model->TITULO.": </span><span style='font-size: 20px;'>".$model->SUBTITULO."</span></b></th>
                       <th align='right'></th>
                      </td>
                    </tr>
                </table>                
                <br><br><br><br><br>
                <table width='100%'>
                    <tr>
                      <td class='contentDetails'>
                       <td align='left' width='50%'></td>                       
                       <td align='justify' style='padding-bottom: 232px;'>                            
                            ".$model->DESCRICAO."
                        </td>                      
                    </tr>
                </table>
                <table width='100%'>
                    <tr>
                      <td class='contentDetails'>
                       <th align='left'></th>
                       <th align='center'><p style='font-size: 20px;'><b>Manaus, AM </b></p></th>                       
                       <th align='right'></th>
                      </td>
                    </tr>
                    <tr>
                      <td class='contentDetails'>
                       <th align='left'></th>                       
                       <th align='center'><p style='font-size: 20px;'><b>2017 </b></p></th>
                       <th align='right'></th>
                      </td>
                    </tr>
                </table>";        
        return $html;
    }
    
    function getSumario($projetoComponente){
        $html = "                                                                
                <table width='100%'>
                    <tr>                      
                       <td align='left'></td>
                       <td align='center'><p style='font-size: 22px;'><b>SUMÁRIO </b></p></td>
                       <td align='right'></td>                      
                    </tr>
                </table>
                <br><br><br><br>";  
        $pag = 6;
        $cont = 0;
        foreach($projetoComponente as $componente){    
            
            if($cont != 0){
                if(!strpos($componente->CODIGO, '.')){
                    $pag++;
                }else{
                    if($numeroLinhas>20){
                        $pag++;
                        $numeroLinhas = 0;
                    }
                }                
            }
            $cont++;
            
            if(!strpos($componente->CODIGO, '.')){
                $numeroLinhas = $this->getNumeroLinhas($componente->DESCRICAO);                               
            }else{
                $numeroLinhas = $this->getNumeroLinhas($componente->DESCRICAO)+$numeroLinhas;                
            }
                        
            if(!strpos($componente->CODIGO, '.')){
                $html .= "<div class='test' style='width:100%;font-size:18px;'>                    
                            <div>                        
                                <div style='float: left; width: 54%;'>
                                    <span style='background-color:#fff;position:relative; right:0px;'>
                                        <b>".$componente->CODIGO."&nbsp;".$componente->TITULO."&nbsp;</b>
                                    </span>
                                </div>                       
                                <div style='float: right; width: 28%;' align='right'>
                                    <span style='background-color:#fff;'>".$pag."</span>
                                </div>                        
                            </div>                    
                        </div>"; 
            }else{
                $html .= "<div class='test' style='width:100%;font-size:18px;'>                    
                            <div>                        
                                <div style='float: left; width: 54%;'>
                                    <span style='background-color:#fff;position:relative; right:0px;'>
                                    ".$componente->CODIGO."&nbsp;".$componente->TITULO."&nbsp;</span>
                                </div>                       
                                <div style='float: right; width: 28%;' align='right'>
                                    <span style='background-color:#fff;'>".$pag."</span>
                                </div>                        
                            </div>                    
                        </div>"; 
            }          
        }                  
        return $html;
    }
    
    function array_sort_by_column($arr, $col, $dir = SORT_ASC) {
        $sort_col = array();
        foreach ($arr as $key=> $row) {
            $sort_col[$key] = $row[$col];
        }
        array_multisort($sort_col, $dir, $arr);
        return $arr;
    } 
    
    function getAtividades($projetoAtividades){        
        $html = "<br><p style='font-size: 22px;'><b> 4 CRONOGRAMA DE ATIVIDADES</b></p>";                               
        $quantidadeTabelas = $this->getQtdTabelas($projetoAtividades);        
        $ano_inicio = $this->getAnoInicio($projetoAtividades);        
        $html .= $this->montaTabelas($quantidadeTabelas, $ano_inicio, $projetoAtividades);                               
        return $html;
    }
    
    function getResponsabilidades($projetoAtividades){
        $html =  "<br><br><p style='font-size: 22px;'> 4.1 Matriz de responsabilidades</p>"; 
        $html .= $this->getSuperiorTabelaResponsabilidades();
        $html .= $this->getAtividadesResponsaveisRow($projetoAtividades);
        $html .= "</table>";
        return $html;
    }
    
    function montaTabelas($quantidadeTabelas, $anoAtual, $projetoAtividades){        
        $html = "";
        for($x = 0; $x < $quantidadeTabelas; $x++){
            $html .= $this->getSuperiorTabela($anoAtual);
            foreach ($projetoAtividades as $projetoAtividade){                        
                $html .= $this->montaAtividade($projetoAtividade, $anoAtual);
            }
            $html .= "</table><br>"; 
            $anoAtual++;
        }
        return $html;
    }
    
    function getAtividadesResponsaveisRow($projetoAtividades){
        $html = "";
        foreach ($projetoAtividades as $projetoAtividade){                        
            $html .= $this->getRowResponsabilidades($projetoAtividade);
        }
        return $html;
    }
    
    function montaAtividade($projetoAtividade, $anoAtual){        
        $date_inicio    =  strtotime($projetoAtividade->DT_INICIO);
        $date_fim       =  strtotime($projetoAtividade->DT_FIM);            
        if(date("Y",$date_inicio) <= $anoAtual && date("Y",$date_fim) >= $anoAtual){                       
            $mes_inicio     = ($anoAtual > date("Y",$date_inicio))?1:date("m",$date_inicio);
            $mes_fim        = ($anoAtual < date("Y",$date_fim))?12:date("m",$date_fim);
            return $this->getPreenchimento($mes_inicio, $mes_fim, $projetoAtividade->getAtividade());
        }
        return "";
    }      
    
    
    function getSuperiorTabela($ano){
        return "<table border=1 style='width:100%;'>"
            ."<tr>"
            ."<th colspan='13'>".$ano."</th>"
            ."</tr>"
            ."<tr>"
            ."<th style='width:40%;'>ATIVIDADE</th>"
            ."<th>JAN</th>"
            ."<th>FEV</th>"
            ."<th>MAR</th>"
            ."<th>ABR</th>"
            ."<th>MAI</th>"
            ."<th>JUN</th>"
            ."<th>JUL</th>"
            ."<th>AGO</th>"
            ."<th>SET</th>"
            ."<th>OUT</th>"
            ."<th>NOV</th>"
            ."<th>DEZ</th>"
            ."</tr>";        
    }
    
    function getSuperiorTabelaResponsabilidades(){
        return "<table border=1 style='width:100%;'>"            
                ."<tr>"
                    ."<th style='width:35%;'>RESPONSÁVEL</th>"
                    ."<th style='width:35%;'>ATIVIDADE</th>"
                    ."<th>DATA INICIAL</th>"
                    ."<th>DATA FINAL</th>"            
                ."</tr>";        
    }
    
    function getRowResponsabilidades($projetoAtividade){        
        return "<tr>"
                    ."<td>".$projetoAtividade->getResponsavel()."</td>"
                    ."<td>".$projetoAtividade->getAtividade()."</td>"
                    ."<td>".$projetoAtividade->getDTINICIO()."</td>"
                    ."<td>".$projetoAtividade->getDTFIM()."</td>"
               ."</tr>"
            ."<tr>";   
    }
    
    function getQtdTabelas($projetoAtividades){
        $data_menor = null;
        $data_maior = null;
        foreach ($projetoAtividades as $atividade){                        
            $date_inicio =  strtotime($atividade->DT_INICIO);
            $date_fim =  strtotime($atividade->DT_FIM);
            $data_menor = ($data_menor == null)?$date_inicio:($data_menor > $date_inicio)?$date_inicio:$data_menor;
            $data_maior = ($date_fim == null)?$date_fim:($data_maior < $date_fim)?$date_fim:$data_maior;            
        }        
        return date("Y",$data_maior) - date("Y",$data_menor) + 1;
    }
    
    function getAnoInicio($projetoAtividades){
        $data_menor = null;        
        foreach ($projetoAtividades as $atividade){                        
            $date_inicio =  strtotime($atividade->DT_INICIO);            
            $data_menor = ($data_menor == null)?$date_inicio:($data_menor > $date_inicio)?$date_inicio:$data_menor;            
        }        
        return date("Y",$data_menor);
    }
    
    function getPreenchimento($ini, $fim, $atividade){
        $retorno = "<tr>";
        $retorno .= "<td style='width:40%;'>".$atividade."</td>";
        for($x = 1; $x <= 12; $x++){
            if($x >= $ini && $x <= $fim){
                $retorno .= "<td style='background-color:red;'></td>";
            }else{
                $retorno .= "<td></td>";
            }
        }
        return $retorno."</tr>";
    }   
    
    public function actionAprovacao()
    {
        $searchModel = new ProjetoSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('aprovacao', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }
}
