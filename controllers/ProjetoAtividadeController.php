<?php

namespace app\controllers;

use Yii;
use app\models\ProjetoAtividade;
use app\models\ProjetoAtividadeSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\db\Expression;

/**
 * ProjetoAtividadeController implements the CRUD actions for ProjetoAtividade model.
 */
class ProjetoAtividadeController extends Controller
{
    /**
     * @inheritdoc
     */
    
    var $parametros;
    
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }
    
    public function beforeAction($action)
    {       
        $session = Yii::$app->session;                 
        if(!$session['logado']){
            return $this->redirect(['site/login']);
        }
        return true;
    }

    /**
     * Lists all ProjetoAtividade models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new ProjetoAtividadeSearch();                
        
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $session['atividades'] = null; 
        return $this->render('index', [
            'searchModel'           => $searchModel,
            'dataProvider'          => $dataProvider,            
        ]);
    }

    /**
     * Displays a single ProjetoAtividade model.
     * @param double $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new ProjetoAtividade model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new ProjetoAtividade();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->ID_PROJETO_ATIVIDADE]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }
    
    public function actionCreateMultiply($id)
    {                                        
        $array_providers = $this->prepara($id);        
        if ($array_providers['model']->load(Yii::$app->request->post())) {                                                
            $array_providers['model']->DT_INICIO   = new Expression("to_date('".implode('-', array_reverse(explode('/', $array_providers['model']->DT_INICIO)))."', 'yyyy-mm-dd')");
            $array_providers['model']->DT_FIM   = new Expression("to_date('".implode('-', array_reverse(explode('/', $array_providers['model']->DT_FIM)))."', 'yyyy-mm-dd')");
            if($array_providers['model']->save()){                
            }else{                
                exit;
            }            
            $model = new ProjetoAtividade();
            $model->ID_PROJETO = $id;
            $array_providers['model'] = $model;
            $array_providers['model']->DT_INICIO    = Yii::$app->formatter->asDate(date('d-m-Y'), 'php:d/m/Y');
            $array_providers['model']->DT_FIM       = Yii::$app->formatter->asDate(date('d-m-Y'), 'php:d/m/Y');
            return $this->render('createMultiply', $array_providers);
        } else {                        
            $array_providers['model']->DT_INICIO    = Yii::$app->formatter->asDate(date('d-m-Y'), 'php:d/m/Y');
            $array_providers['model']->DT_FIM       = Yii::$app->formatter->asDate(date('d-m-Y'), 'php:d/m/Y');
            return $this->render('createMultiply', $array_providers);
        }
    }
    
    public function actionSearch(){
        if (Yii::$app->request->isAjax) {              
            \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;        
            $data = Yii::$app->request->post();            
            $NM_USUARIO = explode(":", $data['NM_USUARIO'])[0];        
            if($NM_USUARIO != "" && $NM_USUARIO != null){                
                $connection = Yii::$app->getDb("db2");
                $command = $connection->createCommand("SELECT "
                    . "                                     CD_USUARIO, "
                    . "                                     NM_USUARIO "
                    . "                                 FROM "
                    . "                                     BD_SICA.USUARIOS "
                    . "                                 WHERE "
                    . "                                     upper(NM_USUARIO) LIKE ('%". strtoupper($NM_USUARIO)."%')");                        
                $result = $command->queryAll();                      
                
            }                                      
            $session = Yii::$app->session; 
            $session['usuarios'] = $result; 
            return "ok";
        }           
    }
    
    public function actionSearchClear(){
        if (Yii::$app->request->isAjax) {                                      
            $session = Yii::$app->session; 
            $session['usuarios'] = null;
            return "ok";
        }           
    }
    
    function filter($item) {
        $mailfilter = Yii::$app->request->getQueryParam('filteremail', '');
        if (strlen($mailfilter) > 0) {
            if (strpos($item['email'], $mailfilter) != false) {
                return true;
            } else {
                return false;
            }
        } else {
            return true;
        }
    }

    /**
     * Updates an existing ProjetoAtividade model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param double $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->ID_PROJETO_ATIVIDADE]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }        

    /**
     * Deletes an existing ProjetoAtividade model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param double $id
     * @return mixed
     */
    public function actionDelete($id, $idProjeto)
    {        
        $this->findModel($id)->delete();
        $array_providers = $this->prepara($idProjeto);        
        return $this->render('createMultiply', $array_providers);
    }

    /**
     * Finds the ProjetoAtividade model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param double $id
     * @return ProjetoAtividade the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = ProjetoAtividade::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
    
    public function actionAdicionaAtividade()
    {        
        if (Yii::$app->request->isAjax) {                        
            $session = Yii::$app->session;                                    
            $atividade = $this->getAtividade(Yii::$app->request->post());               
            $this->adicionaAtividadeNaSessao($atividade, $session);
        }
    }
    
    function getAtividade($data){
        $ID_ATIVIDADE = explode(":", $data['ID_ATIVIDADE'])[0];                                  
        $projetoAtividade = new ProjetoAtividade();        
        $projetoAtividade->ID_ATIVIDADE = $ID_ATIVIDADE;                                    
        return $projetoAtividade;
    } 
    
    function adicionaAtividadeNaSessao($atividade, $session){                                 
        if (!isset($session['atividades']) || count($session['atividades'])==0){                                                
            $session['atividades'] = array($atividade);
        }else{
            if(!$this->existeNoArray($session['atividades'], $atividade->ID_ATIVIDADE)){                
                $arrayAtividades = $session['atividades'];
                $arrayAtividades[] = $atividade;                
                $this->array_sort_by_column($arrayAtividades, 'ID_ATIVIDADE');
                $session['atividades'] = $arrayAtividades;                                
            }
        }         
    }
    
    function existeNoArray($arrayAtividades, $id) {                        
        foreach($arrayAtividades as $val){                 
            if ($val['ID_ATIVIDADE'] === $id){                
                return true;
            }
        }
        return false;
    }
    
    function array_sort_by_column(&$arr, $col, $dir = SORT_ASC) {
        $sort_col = array();
        foreach ($arr as $key=> $row) {            
            $sort_col[$key] = $row[$col];
        }        
        array_multisort($sort_col, $dir, $arr);        
    } 
    
    function prepara($id){
        $model = new ProjetoAtividade();
        $model->ID_PROJETO = $id;
        $modelAtividade = new \app\models\Atividade();    
                
        $dataProviderAtividade = $this->getDataProviderAtividade();
        $providerUsuarios = $this->getProviderUsuarios();
        $providerAtividade = $this->getAtividadesProjeto();
                        
        $searchModel = new ProjetoAtividadeSearch(['ID_PROJETO' => $id]);                        
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $dataProvider->pagination = ['pageSize' => 10];
        $dataProvider->sort->sortParam = false;
        
        return [                
                'model'                     => $model,
                'searchModelAtividade'      => new \app\models\AtividadeSearch(),
                'dataProviderAtividade'     => $dataProviderAtividade,   
                'modelAtividade'            => $modelAtividade, 
                'providerAtividade'         => $providerAtividade,
                'idProjeto'                 => $id,                
                'dataProviderUsuarios'      => $providerUsuarios,                
                'dataProvider'              => $dataProvider,                
              ];
        
    }
    
    function getDataProviderAtividade(){
        $searchModelAtividade = new \app\models\AtividadeSearch();
        $dataProviderAtividade = $searchModelAtividade->search(Yii::$app->request->queryParams);        
        $dataProviderAtividade->pagination = ['pageSize' => 7];
        $dataProviderAtividade->sort->sortParam = false;
        return $dataProviderAtividade;
    }
    
    function getProviderUsuarios(){
            
        $session = Yii::$app->session; 
        $result = $session['usuarios'];                                     

        $providerUsuarios = new \yii\data\ArrayDataProvider([                
                'allModels' => $result,
                'sort' => [
                    'attributes' => ['NM_USUARIO'],
                ],
        ]);                
        
        $providerUsuarios->pagination = ['pageSize' => 10];    
        return $providerUsuarios;
    }
    
    function getAtividadesProjeto(){
        $session = Yii::$app->session; 
        $arrayAtividades = $session['atividades']; 

        $providerAtividade = new \yii\data\ArrayDataProvider([
            'allModels' => $arrayAtividades,            
            'pagination' => [
                'pageSize' => 10,
            ],
        ]); 
        return $providerAtividade;
    }
}
