<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use dosamigos\ckeditor\CKEditorInline;

/* @var $this yii\web\View */
/* @var $model app\models\Projeto */
/* @var $form yii\widgets\ActiveForm */
$this->title = 'Editor de componentes';
$this->params['breadcrumbs'][] = ['label' => 'Projetos', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;

?>
<style>
.sumario{  
  list-style-type: none;
}
.dot-div {
  border-bottom: thin dashed gray;
  width: 100%;
  height: 14px
}
.text-div {
  margin-top: -14px
}
.text-span {
  background: #fff;
  padding-right: 5px
}
.pull-right {
  float: right;
  padding-left: 5px
}
</style>
<ul class="nav nav-tabs">
  <li><?= Html::a('Projeto', ['update', 'id' => $model->ID_PROJETO]) ?></li>
  <li class="active"><a href="#">Componentes</a></li>
  <li><?= Html::a('Atividades', ['projeto-atividade/create-multiply', 'id' => $model->ID_PROJETO]) ?></li>
  <li><?= Html::a('Orçamentos', ['orcamento/create', 'id' => $model->ID_PROJETO]) ?></li>    
</ul>
<br>
<div class="projeto-form">        
    <?php $form = ActiveForm::begin(); ?>    
    <div style="width:60%; margin-right:5%;">        
    <div>
        <label id="capa_toogle">+ Capa</label>        
        <div id="capa_content" style="display: none; background: #fff; border: 1px solid silver;">
            <center>
                <br><br>
                <p style="font-size: 22px;"><b>SÉCULO MANAUS </b></p>
                <br><br><br><br><br>
                <br>                
                <p style="font-size: 18px;"><?php echo $model->RESPONSAVEL; ?> </p>
                <br><br><br><br><br>
                <br><br><br><br><br>                
                <br>
                <p style="font-size: 24px;"><strong><?php echo $model->TITULO; ?> </strong></p>
                <br><br><br><br><br>
                <br><br><br><br><br>
                <br><br><br>
                <p style="font-size: 20px;"><b>Manaus, AM </b></p>
                <p style="font-size: 20px;"><b><?php echo date("Y"); ?> </b></p>
                <br><br><br>
            </center>            
        </div>
    </div> 
    <br>
    <div>
        <label id="sumario_toogle">+ Sumário</label>        
        <div id="sumario_content" style="display: none; background: #fff; border: 1px solid silver;">
            <center>
                <br><br>
                <p style="font-size: 22px;"><b>SUMÁRIO </b></p>
                <br>            
            </center>
            <ul style="width:93%;" class="sumario">
                    <?php 
                    foreach($componentes as $componente){
                    ?>
                        <li>
                            <div class="dot-div"></div>
                            <div class="text-div">
                              <?php 
                              if(!strpos($componente->CODIGO, '.')){                                                                
                              ?>
                                <span class="text-span"><b><?php echo $componente->CODIGO." "."$componente->TITULO" ?></b></span>
                              <?php
                              }else{
                              ?>
                                <span class="text-span"><?php echo $componente->CODIGO." "."$componente->TITULO" ?></span>
                              <?php 
                              }
                              ?>                                
                              <span class="text-span pull-right">x</span>
                            </div>
                        </li>
                    <?php 
                    }
                    ?>                     
            </ul>
            <br><br><br><br><br><br>
            <br><br><br><br><br><br>                
            <br><br><br><br><br>
            <br><br><br><br><br>                       
        </div>
    </div> 
    <div id="componentsform">
        <?php 
        foreach($componentes as $componente){                                       
        ?>    
        <br>
                <label id="capa_toogle_<?php echo $componente->ID_PROJETO_COMPONENTE; ?>" 
                       onclick="tootleDimanic(<?php echo $componente->ID_PROJETO_COMPONENTE; ?>, 
                                              '<?php echo $componente->CODIGO." ".$componente->TITULO; ?>');">+ <?php echo $componente->CODIGO." ".$componente->TITULO; ?></label>
                <div id="capa_content_<?php echo $componente->ID_PROJETO_COMPONENTE; ?>" style="display: none;background: #fff; border: 1px solid silver;padding: 0px;">
                    <?php CKEditorInline::begin(['preset' => 'basic', 'id' => $componente->ID_PROJETO_COMPONENTE]);
                        if($componente->DESCRICAO){
                            echo $componente->DESCRICAO;
                        }else{
                            echo "<br>&nbsp;&nbsp;&nbsp;Clique aqui para editar ".$componente->TITULO."<br><br><br>";
                        }                        
                    CKEditorInline::end();?>
                </div>            
            <br>
        <?php 

        }

        ?>       
    </div>
    <br>
    <div style="margin-left: 10px;margin-right: 10px;">
        <div class="alert alert-success" id="message-success" style="display: none;">                    
            <strong>As alterações foram salvas.</strong>
        </div>
        <div class="alert alert-danger fade in" id="message-error" style="display: none;">                    
            <strong>Erro!</strong> Já existe um componente com esse título.
        </div>  
    </div>
    <table>
        <tr>
            <td>
                <div class="form-group">            
                    <?= Html::button('Salvar Alterações', ['class' => 'btn btn-success', "id" => "salvar"]) ?>  
                </div>
            </td>
            <td>&nbsp;</td>
            <td>
                <div class="form-group">            
                    <?= Html::a('Visualizar Documento', ['/projeto/gerar-pdf', 'id' => $model->ID_PROJETO], ['target' => '_blank', 'class'=>'btn btn-primary']) ; ?>  
                </div>
            </td>
        </tr>
    </table>                
    </div>       

    <?php ActiveForm::end(); ?>

</div>

<script type="text/javascript">   
    var fac= document.getElementById("salvar");
    var visualizar= document.getElementById("visualizar");
    var capa= document.getElementById("capa_toogle");
    var sumario= document.getElementById("sumario_toogle");
    
    var jsonObj = [];    
    fac.onclick = function fun(){               
        $('#componentsform > div').children('.cke_editable').each(function () {
            console.log("tetas");
            componente = {};
            componente[this.id] = this.innerHTML;
            jsonObj.push(componente);
        }); 
        console.log(jsonObj);
        $.ajax({
            url: '<?php echo Yii::$app->request->baseUrl. '/index.php?r=projeto/adiciona-componente' ?>',            
            type: 'post',               
            data: {myData: jsonObj},
            beforeSend: function(){
                
            },
            success:function(data){
                $("#message-success").show();
                    setTimeout(function(){
                        $('#message-success').fadeOut("slow");
                }, 2000);
            }
        });
    };
    
    capa.onclick = function funCapa(){               
        this.innerHTML = (this.innerHTML == "+ Capa")?"- Capa":"+ Capa";
        $('#capa_content').slideToggle("slow");
    };
    
    sumario.onclick = function funSumario(){                       
        this.innerHTML = (this.innerHTML.indexOf('+') > -1)?"- Sumário":"+ Sumário";
        $('#sumario_content').slideToggle("slow");
    };
    
    function tootleDimanic(id, titulo){        
        var titulo_capa= document.getElementById("capa_toogle_"+id);        
        titulo_capa.innerHTML = (titulo_capa.innerHTML == "+ "+titulo) ? "- "+titulo : "+ "+titulo;
        $('#capa_content_'+id).slideToggle("slow");
    }
        
</script>
