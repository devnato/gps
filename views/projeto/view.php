<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Projeto */

$this->title = $model->TITULO;
$this->params['breadcrumbs'][] = ['label' => 'Projetos', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="projeto-view">
   

    <p>
        <?= Html::a('Editar', ['update', 'id' => $model->ID_PROJETO], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Deletar', ['delete', 'id' => $model->ID_PROJETO], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,        
        'attributes' => [            
            'ID_PROJETO',
            'MODELO',
            [
                'attribute' => 'SUBTITULO',
                'value' => $model->SUBTITULO != null ? $model->SUBTITULO : '',       
            ],

            'TITULO',
            'SUBTITULO',
            'DESCRICAO',                        
            [
                'attribute' => 'DT_INICIO',
                'value' => $model->DT_INICIO != null ? $model->DT_INICIO : '',       
            ],            
            [
                'attribute' => 'DT_TERMINO',
                'value' => $model->DT_TERMINO != null ? $model->DT_TERMINO : '',       
            ],                                                          
        ],
    ]) ?>

</div>
