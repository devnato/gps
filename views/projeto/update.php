<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Projeto */

$this->title = 'Atualizar Projeto: ' . $model->TITULO;
$this->params['breadcrumbs'][] = ['label' => 'Projetos', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->ID_PROJETO, 'url' => ['view', 'id' => $model->ID_PROJETO]];
$this->params['breadcrumbs'][] = 'Atualizar';
?>
<div class="projeto-update">    

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
