<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
/* @var $this yii\web\View */
/* @var $searchModel app\models\ProjetoSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Projetos';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="projeto-index">       
    <p>
        <?= Html::a('Criar novo projeto', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
<?php Pjax::begin(); ?>    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'emptyText' => 'Nenhum projeto cadastrado.',
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],                        
            [
                'attribute' => 'MODELO',
                'filter' => false,                
                'format' => 'raw',
                'value' => function ($model) {
                    return  $model->getModelo();
                },
            ],
            'TITULO',
            // 'STATUS',
            // 'DT_APROVACAO',
            // 'LOGIN_APROVACAO',
            // 'DT_INICIO',
            // 'DT_TERMINO',
            // 'PERIODO',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
<?php Pjax::end(); ?></div>
