<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\ProjetoSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="projeto-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'ID_PROJETO') ?>

    <?= $form->field($model, 'ID_MODELO') ?>

    <?= $form->field($model, 'DT_CADASTRO') ?>

    <?= $form->field($model, 'LOGIN_CADASTRO') ?>

    <?= $form->field($model, 'TITULO') ?>

    <?php // echo $form->field($model, 'STATUS') ?>

    <?php // echo $form->field($model, 'DT_APROVACAO') ?>

    <?php // echo $form->field($model, 'LOGIN_APROVACAO') ?>

    <?php // echo $form->field($model, 'DT_INICIO') ?>

    <?php // echo $form->field($model, 'DT_TERMINO') ?>

    <?php // echo $form->field($model, 'PERIODO') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
