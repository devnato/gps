<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
/* @var $this yii\web\View */
/* @var $searchModel app\models\ProjetoSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Projetos';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="projeto-index">       
<?php Pjax::begin(); ?>    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'emptyText' => 'Nenhum projeto cadastrado.',
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],                        
            [
                'attribute' => 'MODELO',
                'filter' => false,                
                'format' => 'raw',
                'value' => function ($model) {
                    return  $model->getModelo();
                },
            ],
            'TITULO',
            'STATUS',
            // 'DT_APROVACAO',
            // 'LOGIN_APROVACAO',
            // 'DT_INICIO',
            // 'DT_TERMINO',
            // 'PERIODO',
            [
                'class' => 'yii\grid\ActionColumn',
                'template' => '{aprovar} {reprovar} {visualizar}',
                'buttons' => [
                    'aprovar' => function ($url2, $model) {                                                                                        
                        return Html::a('<span class="btn btn-success btn-sm glyphicon glyphicon-check"></span>', 'index.php?r=projeto/gerar-pdf&id='.$model->ID_PROJETO, ['title' => 'Aprovar']);
                    },             
                    'reprovar' => function ($url2, $model) {                                                                                        
                        return Html::a('<span class="btn btn-danger btn-sm glyphicon glyphicon-remove"></span>', 'index.php?r=projeto/gerar-pdf&id='.$model->ID_PROJETO, ['title' => 'Não aprovar']);
                    },                    
                    'visualizar' => function ($url2, $model) {                                                
                        return Html::a('', ['/projeto/gerar-pdf', 'id' => $model->ID_PROJETO], ['target' => '_blank', 'class'=>'btn btn-primary btn-sm glyphicon glyphicon-eye-open']) ; 
                    },
                    
                ],                            
            ],
        ],
    ]); ?>
<?php Pjax::end(); ?></div>
