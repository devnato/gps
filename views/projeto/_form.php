<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use app\models\Modelo;
use yii\helpers\ArrayHelper;
use yii\jui\DatePicker;

/* @var $this yii\web\View */
/* @var $model app\models\Projeto */
/* @var $form yii\widgets\ActiveForm */
?>
<ul class="nav nav-tabs" <?= $model->isNewRecord ? 'style="display:none;"' : '' ?>>
  <li class="active"><a href="#">Projeto</a></li>
  <li><?= Html::a('Componentes', ['/projeto/editar-componente', 'id' => $model->ID_PROJETO]) ; ?>  </li>
  <li><?= Html::a('Atividades', ['projeto-atividade/create-multiply', 'id' => $model->ID_PROJETO]) ?></li>  
  <li><?= Html::a('Orçamentos', ['orcamento/create', 'id' => $model->ID_PROJETO]) ?></li>    
</ul>
<br>
<div class="projeto-form">

    <?php $form = ActiveForm::begin(); ?>    
    <div style="width:50%; margin-right:5%;">
        <?= $form->field($model, 'ID_MODELO')->dropDownList(
                ArrayHelper::map(Modelo::find()->all(),'ID_MODELO', 'DC_MODELO')
            );
        ?>

        <?= $form->field($model, 'TITULO')->textInput(['maxlength' => true]) ?>
        
        <?= $form->field($model, 'SUBTITULO')->textInput(['maxlength' => true]) ?>
        
        <?= $form->field($model, 'DESCRICAO')->textarea(['rows' => '3', 'maxlength' => true]) ?>
            
        <?= $form->field($model, 'RESPONSAVEL')->textInput(['maxlength' => true]) ?>                

        <label>Data Início</label><br>
        <?= DatePicker::widget([
            'model' => $model,
            'attribute' => 'DT_INICIO',
            'language' => 'pt',
            'dateFormat' => 'dd/MM/yyyy',
        ]); ?>                
        <br><br>
        <label>Data Término</label><br>
        <?= DatePicker::widget([
            'model' => $model,
            'attribute' => 'DT_TERMINO',
            'language' => 'pt',
            'dateFormat' => 'dd/MM/yyyy',
        ]); ?>           
        <br><br>
        <table>
            <tr>
                <td>
                    <div class="form-group">
                        <?= Html::submitButton($model->isNewRecord ? 'Criar' : 'Alterar', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
                    </div>
                </td>                                
            </tr>
        </table>
    </div>       

    <?php ActiveForm::end(); ?>

</div>
