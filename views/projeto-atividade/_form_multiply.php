<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\grid\GridView;
use yii\widgets\Pjax;
use yii\jui\DatePicker;

/* @var $this yii\web\View */
/* @var $model app\models\Modelo */
/* @var $form yii\widgets\ActiveForm */
?>
<ul class="nav nav-tabs">    
    <li><?= Html::a('Projeto', ['/projeto/update', 'id' => $idProjeto]) ; ?>  </li>  
    <li><?= Html::a('Componentes', ['projeto/editar-componente', 'id' => $idProjeto]) ?></li>
    <li class="active"><a href="#">Atividades</a></li>
    <li><?= Html::a('Orçamentos', ['orcamento/create', 'id' => $idProjeto]) ?></li>    
</ul>
<div class="modelo-form">                  
    <?php $form = ActiveForm::begin(); ?> 
    <?= $form->field($model, 'ID_ATIVIDADE')->hiddenInput()->label(false) ?> 
    <?= $form->field($model, 'RESPONSAVEL')->hiddenInput()->label(false) ?>     
    <?php Pjax::begin(['id' => 'pjax-grid-view-atividades']); ?>            
        <table class="rwd-table">            
            <tr>
                <td colspan="3">                                   
                    <?= $form->field($model, 'ATIVIDADE')->textInput() ?>
                </td>
                <td style="vertical-align: top;">                    
                    <div class="form-group field-projetoatividade-id_atividade required has-error">
                        <label class="control-label">&nbsp;</label>                        
                        &nbsp;
                        <button type="button" class="form-control btn btn-primary glyphicon glyphicon-search" style="border: 0px solid;" data-toggle="modal" data-target="#modalAddAtividade">
                    </div>                                        
                </td>
            </tr>
            <tr>
                <td colspan="3">
                    <?= $form->field($model, 'RESPONSAVELLABEL')->textInput() ?>
                </td>
                <td style="vertical-align: top;">                    
                    <div class="form-group field-projetoatividade-id_atividade required has-error">
                        <label class="control-label">&nbsp;</label>                        
                        &nbsp;<button type="button" class="form-control btn btn-primary glyphicon glyphicon-search" style="border: 0px solid;" data-toggle="modal" data-target="#modalAddResponsavel">
                    </div>                                        
                </td>
            </tr>            
            <tr>
              <td>
                  <label>Data Início</label><br>
                    <?= DatePicker::widget([
                        'model' => $model,
                        'attribute' => 'DT_INICIO',
                        'language' => 'pt',
                        'dateFormat' => 'dd/MM/yyyy',                        
                    ]); ?>        
              <td>&nbsp;</td>
              <td>
                    <label>Data Término</label><br>                                        
                    <?= DatePicker::widget([
                        'model' => $model,
                        'attribute' => 'DT_FIM',
                        'language' => 'pt',                        
                        'dateFormat' => 'dd/MM/yyyy',
                    ]); ?>        
              </td>
            </tr>                        
          </table>
    <br>
    <?= Html::submitButton('Inserir' , ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>    
    <br><br>
    <?= GridView::widget([
        'dataProvider' => $dataProvider, 
        'emptyText' => 'Nenhuma atividade encontrada.',
        'summary'=>'',   
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],            
            [
                'attribute' => 'ID_ATIVIDADE',
                'filter' => false,                
                'format' => 'raw',
                'value' => function ($model) {
                    return  $model->getATIVIDADE();
                },
            ],
            [
                'attribute' => 'RESPONSAVEL',
                'filter' => false,                
                'format' => 'raw',
                'value' => function ($model) {
                    return  $model->getRESPONSAVEL();
                },
            ],
            [
                'attribute' => 'DT_INICIO',
                'filter' => false,                
                'format' => 'raw',
                'value' => function ($model) {
                    return  $model->getDTINICIO();
                },
            ],
            [
                'attribute' => 'DT_FIM',
                'filter' => false,                
                'format' => 'raw',
                'value' => function ($model) {
                    return  $model->getDTFIM();
                },
            ],
            // 'STATUS',            
            [
                'class' => 'yii\grid\ActionColumn',
                'template' => '{delete}',
                'buttons' => [
                    'delete' => function ($url2, $model) {                                                                
                        return Html::a('<span class="glyphicon glyphicon-trash"></span>', ['projeto-atividade/delete', "id"=>$model->ID_PROJETO_ATIVIDADE,"idProjeto"=>$model->ID_PROJETO], ['title' => 'Excluir', 'data-method' => 'POST']);
                    }                                     
                ],                            
            ],
        ],
    ]); ?>
    <?php Pjax::end(); ?>                  

    <?php ActiveForm::end(); ?>

</div>
<?= $this->render('modalAddAtividade', [        
        'searchModelAtividade'      => $searchModelAtividade,
        'dataProvider'              => $dataProviderAtividade,    
        'modelAtividade'            => $modelAtividade,  
    ]);   
?> 
<?= 
    $this->render('modalAddResponsavel',[        
        'dataProviderUsuarios'      => $dataProviderUsuarios,                
        'modelAtividade'            => $modelAtividade,  
    ]);
?> 
<script type="text/javascript"> 
    function removerComponente(valor){
        var param = JSON.parse(valor);        
        
        $.ajax({
            url: '<?php echo Yii::$app->request->baseUrl. '/index.php?r=modelo/remove-componente' ?>',            
            type: 'post',   
            data: {
                 //ID_COMPONENTE: param.ID_COMPONENTE , 
                 TITULO: param.TITULO,
             },            
            success:function(data){
                console.log(data);
                $.pjax({container: '#pjax-grid-view-componentes'});                    
            }
        });
    }           
</script>