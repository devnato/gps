<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\ProjetoAtividade */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="projeto-atividade-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'ID_PROJETO_ATIVIDADE')->textInput() ?>

    <?= $form->field($model, 'DT_INICIO')->textInput() ?>

    <?= $form->field($model, 'DT_FIM')->textInput() ?>

    <?= $form->field($model, 'ID_ATIVIDADE')->textInput() ?>

    <?= $form->field($model, 'ID_PROJETO')->textInput() ?>

    <?= $form->field($model, 'STATUS')->textInput(['maxlength' => true]) ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
