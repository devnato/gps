<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\ProjetoAtividadeSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="projeto-atividade-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'ID_PROJETO_ATIVIDADE') ?>

    <?= $form->field($model, 'DT_INICIO') ?>

    <?= $form->field($model, 'DT_FIM') ?>

    <?= $form->field($model, 'ID_ATIVIDADE') ?>

    <?= $form->field($model, 'ID_PROJETO') ?>

    <?php // echo $form->field($model, 'STATUS') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
