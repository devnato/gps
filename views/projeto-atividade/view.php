<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\ProjetoAtividade */

$this->title = $model->ID_PROJETO_ATIVIDADE;
$this->params['breadcrumbs'][] = ['label' => 'Projeto Atividades', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="projeto-atividade-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->ID_PROJETO_ATIVIDADE], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->ID_PROJETO_ATIVIDADE], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'ID_PROJETO_ATIVIDADE',
            'DT_INICIO',
            'DT_FIM',
            'ID_ATIVIDADE',
            'ID_PROJETO',
            'STATUS',
        ],
    ]) ?>

</div>
