<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\grid\GridView;
use yii\widgets\Pjax;

?>  
<!-- Modal -->
<div class="modal fade bs-example-modal-lg" tabindex="-1" id="modalAddResponsavel" role="dialog" aria-labelledby="Adicionar Responsavel">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <h4 class="modal-title" id="myModalLabel">Selecionar Responsável</h4>
            </div>            
            <div class="modal-body">
                <?php $form = ActiveForm::begin(); ?>
                <div style="width:100%;height: 40px;">                    
                        <div style="width:70%;float: left;">
                            <input type="text" id="NM_USUARIO" style="width: 100%;">
                        </div>                    
                        <div style="width:1%;float: left;height: 1px;">                            
                        </div>
                        <div style="width:9%;float: left;">                                  
                            <?= Html::button('Pesquisar', ['class' => 'btn btn-success', "id" => "pesquisar"]) ?>                                                
                        </div>                                                   
                </div> 
                <?php ActiveForm::end(); ?>
                <?php Pjax::begin(['id' => 'pjax-grid-view-responsavel','timeout' => 50000]); ?>
                <?= GridView::widget([
                    'dataProvider' => $dataProviderUsuarios,                                        
                    'summary' => '',
                    'columns' => [                                  
                        [
                            "attribute" => "Usuario",                            
                            'value' => 'NM_USUARIO',
                            'headerOptions' => ['style' => 'width:80%'],                            
                        ],
                        [
                            'class' => 'yii\grid\ActionColumn',
                            'template' => '{delete}',                                                        
                            'buttons' => [
                                'delete' => function ($model, $key) { 
                                    return Html::button(' Selecionar', ['value' => json_encode(array('CD_USUARIO' => $key['CD_USUARIO'], 'NM_USUARIO' => $key['NM_USUARIO'])), 'class' => 'btn btn-primary btn-sm glyphicon glyphicon-check', 'onclick'=>'selecionarResponsavel(value)']);
                                }                                     
                            ],                            
                        ],                      
                    ],
                ]); ?>
                <?php Pjax::end(); ?>
            </div>
            
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Fechar</button>                 
            </div>
    </div>
  </div>
</div>
<script type="text/javascript">       
    function selecionarResponsavel(valor){
        var param = JSON.parse(valor);        
        $('#projetoatividade-responsavellabel').val(param.NM_USUARIO);
        $('#projetoatividade-responsavel').val(param.CD_USUARIO);       
        $('#modalAddResponsavel').modal('toggle');   
        $.ajax({
            url: '<?php echo Yii::$app->request->baseUrl. '/index.php?r=projeto-atividade/search-clear' ?>',            
            type: 'post',                           
            success:function(data){                                                
                $.pjax.reload({                                
                    container:"#pjax-grid-view-responsavel",
                    async: false
               }); 
            }
        });
    }
</script>
<script type="text/javascript">   
    var fac= document.getElementById("pesquisar");    
        
    fac.onclick = function fun(){                               
        
        $.ajax({
            url: '<?php echo Yii::$app->request->baseUrl. '/index.php?r=projeto-atividade/search' ?>',            
            type: 'post',               
            data: {
                 NM_USUARIO: $("#NM_USUARIO").val() ,                                   
            },            
            success:function(data){                                
               $.pjax.reload({                                
                    container:"#pjax-grid-view-responsavel",
                    async: false
               }); 
            }
        });
    };
        
</script>
