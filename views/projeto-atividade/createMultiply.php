<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\ProjetoAtividade */

$this->title = 'Adicionar Atividades';
$this->params['breadcrumbs'][] = ['label' => 'Projetos', 'url' => ['/projeto']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="projeto-atividade-create">    

    <?= $this->render('_form_multiply', [
        'model'                     => $model,
        'searchModelAtividade'      => $searchModelAtividade,
        'dataProviderAtividade'     => $dataProviderAtividade,    
        'modelAtividade'            => $modelAtividade,  
        'providerAtividade'         => $providerAtividade,
        'idProjeto'                 => $idProjeto,
        'dataProviderUsuarios'      => $dataProviderUsuarios,                
        'dataProvider'              => $dataProvider,        
    ]) ?>

</div>
