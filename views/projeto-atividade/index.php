<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\ProjetoAtividadeSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Projeto Atividades';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="projeto-atividade-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Projeto Atividade', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'ID_PROJETO_ATIVIDADE',
            'DT_INICIO',
            'DT_FIM',
            'ID_ATIVIDADE',
            'ID_PROJETO',
            // 'STATUS',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
