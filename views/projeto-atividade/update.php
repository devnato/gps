<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\ProjetoAtividade */

$this->title = 'Update Projeto Atividade: ' . $model->ID_PROJETO_ATIVIDADE;
$this->params['breadcrumbs'][] = ['label' => 'Projeto Atividades', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->ID_PROJETO_ATIVIDADE, 'url' => ['view', 'id' => $model->ID_PROJETO_ATIVIDADE]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="projeto-atividade-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
