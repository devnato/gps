<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Atividade */

$this->title = 'Update Atividade: ' . $model->ID_ATIVIDADE;
$this->params['breadcrumbs'][] = ['label' => 'Atividades', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->ID_ATIVIDADE, 'url' => ['view', 'id' => $model->ID_ATIVIDADE]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="atividade-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
