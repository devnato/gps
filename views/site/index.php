<?php

use yii\helpers\Html;

/* @var $this yii\web\View */

$this->title = 'Gestor de Projetos Seculo';
?>
<div class="site-index">

    <div class="jumbotron">        
        <br><br>
        <br><br>
        <p>
            <?= Html::a('Cadastrar Novo Projeto', ['/projeto/create'], ['class'=>'btn btn-lg btn-success']) ?>            
    </div>
</div>
