<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Componente */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="componente-form">

    <?php $form = ActiveForm::begin(); ?>    

    <?= $form->field($model, 'TITULO')->textInput(['maxlength' => true]) ?>    

    <div class="form-group">
        <?= Html::button($model->isNewRecord ? 'Criar' : 'Alterar', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary', "id" => "inserir"]) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
<script type="text/javascript">
    var fac= document.getElementById("inserir");
    fac.onclick = function fun(){
        $.ajax({
            url: '<?php echo Yii::$app->request->baseUrl. '/index.php?r=componente/inserir' ?>',            
            type: 'post',   
            data: {
                 componenteNome: $("#componente-titulo").val() ,                                   
             },
            success:function(data){
                console.log(data);
            }
        });
    };
</script>