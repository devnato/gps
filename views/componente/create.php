<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\Componente */

$this->title = 'Criar Componente';
$this->params['breadcrumbs'][] = ['label' => 'Componentes', 'url' => ['index']];
$this->params['breadcrumbs'][] = "Criar componente";
?>
<div class="componente-create">    

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
