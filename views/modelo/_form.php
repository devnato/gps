<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\grid\GridView;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $model app\models\Modelo */
/* @var $form yii\widgets\ActiveForm */
?>
<script type="text/javascript">       
    function insereComponente(){
        $.ajax({
            url: '<?php echo Yii::$app->request->baseUrl. '/index.php?r=componente/inserir' ?>',            
            type: 'post',   
            data: {
                 componenteNome: $("#componente-titulo").val() ,                                   
             },
            success:function(data){                                
                if(data === "existe"){
                    $("#message-error").show();
                    setTimeout(function(){
                        $('#message-error').fadeOut(3000);
                    }, 2000);
                }
                if(data === "branco"){
                    $("#message-blank").show();
                    setTimeout(function(){
                        $('#message-blank').fadeOut(3000);
                    }, 2000);
                }
                if(data === "ok"){
                    $("#message-success").show();
                    setTimeout(function(){
                        $('#message-success').fadeOut(3000);
                    }, 2000);
                }
                                                
                $("#componente-titulo").val("");                
                $.pjax.reload({                                
                    container:"#componentes_list",                                
                    async: false
                });
            }
        });
    };
    function selecionarComponente(valor){
        var param = JSON.parse(valor);        
        idParent = $('#parentComponent').val();        
        codeParent = $('#codeParentComponent').val();        
                    
        $.ajax({
            url: '<?php echo Yii::$app->request->baseUrl. '/index.php?r=modelo/adiciona-componente' ?>',            
            type: 'post',   
            data: {
                 ID_COMPONENTE: param.ID_COMPONENTE, 
                 TITULO: param.TITULO,
                 ID_PARENT: idParent,
                 CODE_PARENT: codeParent,
             },            
            success:function(data){
                console.log(data);
                
                $.pjax.reload({                                
                                container:"#pjax-grid-view-componentes",  
                                async: false
                });
                $('#modalAddComponente').modal('toggle');
            }
        });
    }
</script>
<div class="modelo-form">
    <br>
    <?php $form = ActiveForm::begin(); ?> 
    <input type="hidden" id="parentComponent" value="">
    <input type="hidden" id="codeParentComponent" value="">
    <?php Pjax::begin(['id' => 'pjax-grid-view-componentes','timeout' => 50000]); ?>
    <?=   GridView::widget([
                        'dataProvider' => $providerComponente,
                        'summary'=>'',    
                        'emptyText' => 'Sem componentes.',
                        'columns' => [                                           
                            [
                                'attribute' => 'CODIGO',
                                'label' => "CÓDIGO",                                   
                                'headerOptions' => ['style' => 'width:10%;'],
                            ],
                            [
                                'attribute' => 'TITULO',
                                'filter' => false,
                                'headerOptions' => ['style' => 'width:70%;'],
                                'format' => 'raw',
                                 'value' => function ($model) {
                                     return  $model->getTitulo();
                                 },
                            ],
                            [
                                'class' => 'yii\grid\ActionColumn',
                                'template' => '{addcomponente} {delete}',
                                'buttons' => [
                                    'delete' => function ($url2, $model) {                                                                
                                        return Html::button('', ['value' => json_encode(array('ID_COMPONENTE' => $model->ID_COMPONENTE, 'TITULO' => $model->TITULO)), 'class' => 'btn btn-danger btn-sm fa fa-trash-o', 'onclick'=>'removerComponente(value)']);
                                    },
                                    'addcomponente' => function ($url2, $model) {                                                                
                                        return Html::button(' SUB COMPONENTE', ['value' => json_encode(array('ID_COMPONENTE' => $model->ID_COMPONENTE, 'TITULO' => $model->TITULO)), 'class' => 'btn btn-primary btn-sm fa fa-plus', 'onclick'=>'addSubComponente('.$model->ID_COMPONENTE.',"'.$model->CODIGO.'")']);
                                    } 
                                ],                            
                            ],
                        ],
    ]); ?> 
    <?php Pjax::end(); ?>    
    <button type="button" class="btn btn-primary glyphicon glyphicon-plus" onclick="addComponente();" data-toggle="modal" data-target="#modalAddComponente">
        COMPONENTE
    </button>  
    <br><br><br>
    <?= $form->field($model, 'DC_MODELO')->textInput(['maxlength' => true]) ?>

    <div class="form-group">
        <table>
            <tr>
                <td>
                    <?= Html::submitButton($model->isNewRecord ? 'Salvar' : 'Alterar', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
                </td>
                <td>&nbsp;</td>
                <td>
                    <?= Html::a('Cancelar', ['index'], ['class' => 'btn btn-warning']) ?>
                </td>
            </tr>
        </table>                
    </div>        
    <?php ActiveForm::end(); ?>

</div>
<?= $this->render('modalAddComponente', [        
        'searchModelComponente'    => $searchModelComponente,
        'dataProvider'             => $dataProviderComponente,    
        'modelComponente'          => $modelComponente,  
    ]) 
?> 
<script type="text/javascript"> 
    function removerComponente(valor){
        var param = JSON.parse(valor);        
        
        $.ajax({
            url: '<?php echo Yii::$app->request->baseUrl. '/index.php?r=modelo/remove-componente' ?>',            
            type: 'post',   
            data: {
                 ID_COMPONENTE: param.ID_COMPONENTE , 
                 TITULO: param.TITULO,
             },            
            success:function(data){
                console.log(data);
                $.pjax({container: '#pjax-grid-view-componentes'});                    
            }
        });
    }
    function addComponente(){                
        $('#parentComponent').val("");
        $('#codeParentComponent').val("");        
    }
    function addSubComponente(id, codigo){        
        console.log(id+" , "+ codigo);
        $('#modalAddComponente').modal('toggle');
        $('#parentComponent').val(id);
        $('#codeParentComponent').val(codigo);        
    }
    
    
    
</script>