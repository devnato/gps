<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\grid\GridView;
use yii\widgets\Pjax;

?>  
<!-- Modal -->
<div class="modal fade bs-example-modal-lg" tabindex="-1" id="modalAddComponente" role="dialog" aria-labelledby="modalAddComponente">    
    <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <h4 class="modal-title" id="myModalLabel">Selecionar Componente</h4>
            </div>
            <div style="margin-left: 10px;margin-right: 10px;">
                <div class="alert alert-success" id="message-success" style="display: none;">                    
                    <strong>Componente cadastrado.</strong>
                </div>
                <div class="alert alert-danger fade in" id="message-error" style="display: none;">                    
                    <strong>Erro!</strong> Já existe um componente com esse título.
                </div>
                <div class="alert alert-danger fade in" id="message-blank" style="display: none;">                    
                    <strong>Erro!</strong> Digite o nome do componente.
                </div> 
            </div>
            <div class="modal-body">
                
                <?php $form = ActiveForm::begin(); ?>                 
                
                <div style="width:100%;height: 40px;">                    
                    <div style="width:70%;float: left;">
                            <?= 
                            $form->field($modelComponente, 'TITULO',[
                                'template' => '<div>
                                <div>{input}</div>
                            </div>'])->input('email', ['placeholder' => "Criar novo componente"])->label(false);
                        ?>                            
                    </div>                    
                    <div style="width:1%;float: left;height: 1px;">                            
                    </div>
                    <div style="width:9%;float: left;">  
                        <?= Html::button('Criar', ['class' => 'btn btn-success', "id" => "inserir"]) ?>                        
                    </div>                           
                    <div id="loading" style="float: left; width: 35px; height: 35px;display: none;">
                        <?= Html::img('@web/spinner.gif', ['alt'=>'some', 'class'=>'thing', 'width' => '40px']);?>
                    </div>
                </div>                                                                                                
                <?php ActiveForm::end(); ?>                                                                               
                <?php Pjax::begin(['id' => 'componentes_list','timeout' => 50000]); ?> 
                    <?=     GridView::widget([
                        'dataProvider' => $dataProvider,
                        'summary'=>'',      
                        'filterModel' => $searchModelComponente, 
                        'emptyText' => 'Nenhum componente encontrado.',
                        'columns' => [                                                        
                            [
                                'attribute' => 'TITULO',
                                'header' => 'Lista de componentes',  
                                'headerOptions' => ['style' => 'width:80%'],                              
                                'filterInputOptions' => [
                                    'class' => 'form-control',
                                    'placeholder' => 'Pesquisar componente'
                                ]
                            ],                          
                            [
                                'class' => 'yii\grid\ActionColumn',
                                'template' => '{delete}',
                                'buttons' => [
                                    'delete' => function ($url2, $model) {                                                                
                                        return Html::button(' Selecionar', ['value' => json_encode(array('ID_COMPONENTE' => $model->ID_COMPONENTE, 'TITULO' => $model->TITULO)), 'class' => 'btn btn-primary btn-sm glyphicon glyphicon-check', 'onclick'=>'selecionarComponente(value)']);
                                    }                                     
                                ],                            
                            ],
                        ],
                    ]); ?>                
                <?php Pjax::end(); ?>
                
            </div>        
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Fechar</button>                 
            </div>
    </div>
  </div>
</div>
<script type="text/javascript">   
    var fac= document.getElementById("inserir");
    fac.onclick = function fun(){
        insereComponente();
    };

</script>