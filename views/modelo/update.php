<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Modelo */

$this->title = 'Atualizar: ' . $model->DC_MODELO;
$this->params['breadcrumbs'][] = ['label' => 'Modelos', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->ID_MODELO, 'url' => ['view', 'id' => $model->ID_MODELO]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="modelo-update">   

    <?= $this->render('_form', [
        'model' => $model,
        'searchModelComponente'     => $searchModelComponente,
        'dataProviderComponente'    => $dataProviderComponente,    
        'modelComponente'           => $modelComponente,  
        'providerComponente'        => $providerComponente,
    ]) ?>

</div>
