<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\Modelo */

$this->title = 'Criar Modelo';
$this->params['breadcrumbs'][] = ['label' => 'Modelos', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="modelo-create">    

    <?= $this->render('_form', [
        'model' => $model,
        'searchModelComponente'     => $searchModelComponente,
        'dataProviderComponente'    => $dataProviderComponente,    
        'modelComponente'           => $modelComponente,  
        'providerComponente'        => $providerComponente,
    ]) ?>

</div>
