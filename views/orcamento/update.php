<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Orcamento */

$this->title = 'Update Orcamento: ' . $model->ID_ORCAMENTO;
$this->params['breadcrumbs'][] = ['label' => 'Orcamentos', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->ID_ORCAMENTO, 'url' => ['view', 'id' => $model->ID_ORCAMENTO]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="orcamento-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
