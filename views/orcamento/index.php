<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\OrcamentoSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Orcamentos';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="orcamento-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Orcamento', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'ID_PROJETO',
            'ID_GASTO',
            'QUANTIDADE',
            'VALOR_UNITARIO',
            'VALOR_TOTAL',
            // 'TIPO',
            // 'ID_ORCAMENTO',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
