<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\grid\GridView;
use yii\widgets\Pjax;
use kartik\money\MaskMoney;

/* @var $this yii\web\View */
/* @var $model app\models\Modelo */
/* @var $form yii\widgets\ActiveForm */
?>
<ul class="nav nav-tabs">    
    <li><?= Html::a('Projeto', ['/projeto/update', 'id' => $idProjeto]) ; ?>  </li>  
    <li><?= Html::a('Componentes', ['projeto/editar-componente', 'id' => $idProjeto]) ?></li>
    <li><?= Html::a('Atividades', ['projeto-atividade/create-multiply', 'id' => $idProjeto]) ?></li> 
    <li class="active"><a href="#">Orçamentos</a></li>
</ul>
<div class="modelo-form">                  
    <?php $form = ActiveForm::begin(); ?>
    <?= $form->field($model, 'ID_GASTO')->hiddenInput()->label(false) ?>
    <?= $form->field($model, 'ID_TIPO_GASTO')->hiddenInput()->label(false) ?>
    <br>
    <?php Pjax::begin(['id' => 'pjax-grid-view-atividades']); ?>            
        <table class="rwd-table" >
            <tr>
                <td colspan="3">                                   
                    <?= $form->field($model, 'GASTO')->textInput(['style'=>'width:350px;']) ?>
                </td>
                <td style="vertical-align: top;">                    
                    <div class="form-group field-projetoatividade-id_atividade required has-error">
                        <label class="control-label">&nbsp;</label>                        
                        &nbsp;
                        <button type="button" class="form-control btn btn-primary glyphicon glyphicon-search" style="border: 0px solid;" data-toggle="modal" data-target="#modalAddGasto">
                    </div>                                        
                </td>
            </tr>
            <tr>
                <td colspan="3">  
                    <?= $form->field($model, 'CATEGORIA')->textInput(['style'=>'width:350px;']) ?>
                </td>
                <td style="vertical-align: top;">                    
                    <div class="form-group field-projetoatividade-id_atividade required has-error">
                        <label class="control-label">&nbsp;</label>                        
                        &nbsp;<button type="button" class="form-control btn btn-primary glyphicon glyphicon-search" style="border: 0px solid;" data-toggle="modal" data-target="#modalAddCategoria">
                    </div>                                        
                </td>
            </tr>            
            <tr>
              <td>
                <div class="form-group field-orcamento-quantidade">                    
                    <?= $form->field($model, 'VALOR_UNITARIO')->widget('kartik\money\MaskMoney', [
                        // configure additional widget properties here
                    ]) ?>
                    <div class="help-block"></div>
                </div>                                                                                  
              <td>&nbsp;</td>
              <td>                                    
              </td>
              <td>
                  <?=$form->field($model, 'QUANTIDADE', [                                               
                            ])->textInput([
                                 'type' => 'number',
                                 'style'=>'width:80px;'
                            ])?>                  
              </td>
            </tr> 
            <tr>
              <td>
                <?= $form->field($model, 'TIPO')->radioList([1 => 'Despesa', 0 => 'Receita'])->label(false); ?>  
              <td>&nbsp;</td>
              <td>                                    
              </td>
              <td>                
              </td>
            </tr> 
          </table>    
    <?= Html::submitButton('Inserir' , ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>    
    <br><br>
    <?= GridView::widget([
        'dataProvider' => $dataProvider, 
        'emptyText' => 'Nenhuma atividade encontrada.',
        'summary'=>'',   
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],            
            [
                'attribute' => 'ID_GASTO',
                'filter' => false,                
                'format' => 'raw',
                'value' => function ($model) {
                    return  $model->getGASTO();
                },
            ],
            [
                'attribute' => 'ID_TIPO_GASTO',
                'filter' => false,                
                'format' => 'raw',
                'value' => function ($model) {
                    return  $model->getCATEGORIA();
                },
            ],
            [
                'attribute' => 'VALOR_UNITARIO',
                'filter' => false,                
                'format' => 'raw',
                'value' => function ($model) {
                    return  $model->getVALOR_UNITARIO_REAL();
                },
            ],
            'QUANTIDADE', 
            [
                'attribute' => 'VALOR_TOTAL',
                'filter' => false,                
                'format' => 'raw',
                'value' => function ($model) {
                    return  $model->getVALOR_TOTAL_REAL();
                },
            ],                       
            [
                'class' => 'yii\grid\ActionColumn',
                'template' => '{delete}',
                'buttons' => [
                    'delete' => function ($url2, $model) {                                                                
                        return Html::a('<span class="glyphicon glyphicon-trash"></span>', ['projeto-atividade/delete', "id"=>$model->ID_ORCAMENTO,"idProjeto"=>$model->ID_PROJETO], ['title' => 'Excluir', 'data-method' => 'POST']);
                    }                                     
                ],                            
            ],
        ],
    ]); ?>
    <?php Pjax::end(); ?>                  

    <?php ActiveForm::end(); ?>

</div>
<?= $this->render('modalAddGasto',[
    'searchModelGasto'          => $searchModelGasto,
    'dataProviderGasto'         => $dataProviderGasto,  
    'modelGasto'                => $modelGasto,
]);   
?>
<?= $this->render('modalAddCategoria',[
    'searchModelCategoria'          => $searchModelCategoria,
    'dataProviderCategoria'         => $dataProviderCategoria,  
    'modelCategoria'                => $modelCategoria,
]);   
?>
<script type="text/javascript"> 
    function removerComponente(valor){
        var param = JSON.parse(valor);        
        
        $.ajax({
            url: '<?php echo Yii::$app->request->baseUrl. '/index.php?r=modelo/remove-componente' ?>',            
            type: 'post',   
            data: {
                 //ID_COMPONENTE: param.ID_COMPONENTE , 
                 TITULO: param.TITULO,
             },            
            success:function(data){
                console.log(data);
                $.pjax({container: '#pjax-grid-view-componentes'});                    
            }
        });
    }           
</script>