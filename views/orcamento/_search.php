<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\OrcamentoSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="orcamento-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'ID_PROJETO') ?>

    <?= $form->field($model, 'ID_GASTO') ?>

    <?= $form->field($model, 'QUANTIDADE') ?>

    <?= $form->field($model, 'VALOR_UNITARIO') ?>

    <?= $form->field($model, 'VALOR_TOTAL') ?>

    <?php // echo $form->field($model, 'TIPO') ?>

    <?php // echo $form->field($model, 'ID_ORCAMENTO') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
