<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Orcamento */
/* @var $form ActiveForm */
?>
<div class="orcamento-forms">

    <?php $form = ActiveForm::begin(); ?>

        <?= $form->field($model, 'ID_PROJETO') ?>
        <?= $form->field($model, 'ID_GASTO') ?>
        <?= $form->field($model, 'QUANTIDADE') ?>
        <?= $form->field($model, 'VALOR_UNITARIO') ?>
        <?= $form->field($model, 'VALOR_TOTAL') ?>
        <?= $form->field($model, 'TIPO') ?>
    
        <div class="form-group">
            <?= Html::submitButton('Submit', ['class' => 'btn btn-primary']) ?>
        </div>
    <?php ActiveForm::end(); ?>

</div><!-- orcamento-forms -->
