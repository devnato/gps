<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\grid\GridView;
use yii\widgets\Pjax;

?>  
<!-- Modal -->
<div class="modal fade bs-example-modal-lg" tabindex="-1" id="modalAddCategoria" role="dialog" aria-labelledby="Adicionar Categoria">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <h4 class="modal-title" id="myModalLabel">Selecionar Categoria</h4>
            </div>
            <div style="margin-left: 10px;margin-right: 10px;">
                <div class="alert alert-success" id="message-success" style="display: none;">                    
                    <strong>Categoria cadastrada.</strong>
                </div>
                <div class="alert alert-danger fade in" id="message-error" style="display: none;">                    
                    <strong>Erro!</strong> Categoria já cadastrada.
                </div>  
                <div class="alert alert-danger fade in" id="message-blank" style="display: none;">                    
                    <strong>Erro!</strong> Digite o nome da categoria.
                </div>  
            </div>
            <div class="modal-body">
                
                <?php $form = ActiveForm::begin(); ?>                 
                
                <div style="width:100%;height: 40px;">                    
                    <div style="width:70%;float: left;">
                         <?= 
                                $form->field($modelCategoria, 'DC_TIPO_GASTO',[
                                'template' => ' <div>
                                                    <div>{input}</div>
                                                </div>'])->input('email', ['placeholder' => "Criar nova categoria"])->label(false);
                        ?>                             
                    </div>                    
                    <div style="width:1%;float: left;height: 1px;">                            
                    </div>
                    <div style="width:9%;float: left;">                                  
                        <?= Html::button('Criar', ['class' => 'btn btn-success', "id" => "inserir-categoria"]) ?>                                                
                    </div>                           
                    <div id="loading" style="float: left; width: 35px; height: 35px;display: none;">
                        <?= Html::img('@web/spinner.gif', ['alt'=>'some', 'class'=>'thing', 'width' => '40px']);?>
                    </div>
                </div>                                                                                                
                <?php ActiveForm::end(); ?>                                                                               
                <?php Pjax::begin(['id' => 'pjax-grid-view-categoria','timeout' => 50000]); ?> 
                    <?=     GridView::widget([
                        'dataProvider' => $dataProviderCategoria,
                        'summary'=>'',   
                        'filterModel' => $searchModelCategoria,    
                        'emptyText' => 'Nenhum gasto encontrada.',
                        'columns' => [                                                        
                            [
                                'attribute' => 'DC_TIPO_GASTO',
                                'header' => 'Lista de Categoria',  
                                'headerOptions' => ['style' => 'width:80%'],
                                'filterInputOptions' => [
                                    'class'       => 'form-control',
                                    'placeholder' => 'Pesquisar gastos'
                                ]
                            ],                          
                            [
                                'class' => 'yii\grid\ActionColumn',
                                'template' => '{delete}',
                                'buttons' => [
                                    'delete' => function ($url2, $model) {                                                                
                                        return Html::button(' Selecionar', 
                                                    ['value' => json_encode(array(
                                                                                    'ID_CATEGORIA' => $model->ID_CATEGORIA,
                                                                                    'DC_TIPO_GASTO' => $model->DC_TIPO_GASTO
                                                                                 )
                                                                            ), 
                                                     'class' => 'btn btn-primary btn-sm glyphicon glyphicon-check', 
                                                     'onclick'=>'selecionarCategoria(value)']);
                                    }                                     
                                ],                            
                            ],
                        ],
                    ]); ?>                
                <?php Pjax::end(); ?>
                
            </div>
            
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Fechar</button>                 
            </div>
    </div>
  </div>
</div>
<script type="text/javascript">   
    var fac= document.getElementById("inserir-categoria");
    fac.onclick = function fun2(){        
        $.ajax({
            url: '<?php echo Yii::$app->request->baseUrl. '/index.php?r=categoria/inserir' ?>',            
            type: 'post',   
            data: {
                 DC_TIPO_GASTO: $("#categoria-dc_tipo_gasto").val() ,                                   
             },
            beforeSend: function(){
                $("#loading").fadeIn();                
            },
            success:function(data){                
                
                console.log(data);
                
                if(data === "existe"){
                    $("#message-error").show();
                    setTimeout(function(){
                        $('#message-error').fadeOut(3000);
                    }, 2000);
                }
                if(data === "branco"){
                    $("#message-blank").show();
                    setTimeout(function(){
                        $('#message-blank').fadeOut(3000);
                    }, 2000);
                }
                if(data === "ok"){
                    $("#message-success").show();
                    setTimeout(function(){
                        $('#message-success').fadeOut(3000);
                    }, 2000);
                }
                                
                $("#loading").hide();
                $("#componente-titulo").val("");                                
                $.pjax.reload({                                
                    container:"#pjax-grid-view-categoria",
                    async: false
                });
            }
        });
    };
    function selecionarCategoria(valor){
        var param = JSON.parse(valor);                    
        $('#orcamento-categoria').val(param.DC_TIPO_GASTO);
        $('#orcamento-id_tipo_gasto').val(param.ID_CATEGORIA);       
        $('#modalAddCategoria').modal('toggle');                    
    }
</script>