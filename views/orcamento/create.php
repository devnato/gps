<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\ProjetoAtividade */

$this->title = 'Adicionar Atividades';
$this->params['breadcrumbs'][] = ['label' => 'Projetos', 'url' => ['/projeto']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="projeto-atividade-create">    

    <?= $this->render('_form', [
        'model'                     => $model,        
        'idProjeto'                 => $idProjeto,     
        'searchModelGasto'          => $searchModelGasto,
        'dataProviderGasto'         => $dataProviderGasto, 
        'modelGasto'                => $modelGasto,
        'searchModelCategoria'      => $searchModelCategoria,
        'dataProviderCategoria'     => $dataProviderCategoria, 
        'modelCategoria'            => $modelCategoria,
        'dataProvider'              => $dataProvider,
    ]) ?>
</div>
