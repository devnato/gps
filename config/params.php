<?php

return [
    'adminEmail' => 'admin@example.com',
    'maskMoneyOptions' => [
        'prefix' => 'R$ ',        
        'affixesStay' => true,
        'thousands' => '.',
        'decimal' => ',',
        'precision' => 2, 
        'allowZero' => false,
        'allowNegative' => false,
    ]
];
