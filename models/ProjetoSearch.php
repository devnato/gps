<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Projeto;

/**
 * ProjetoSearch represents the model behind the search form about `app\models\Projeto`.
 */
class ProjetoSearch extends Projeto
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['ID_PROJETO', 'ID_MODELO', 'DT_CADASTRO'], 'number'],
            [['LOGIN_CADASTRO', 'TITULO', 'DT_APROVACAO', 'LOGIN_APROVACAO', 'DT_INICIO', 'DT_TERMINO', 'PERIODO'], 'safe'],
            [['STATUS'], 'integer'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Projeto::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'ID_PROJETO' => $this->ID_PROJETO,
            'ID_MODELO' => $this->ID_MODELO,
            'DT_CADASTRO' => $this->DT_CADASTRO,
            'STATUS' => $this->STATUS,
        ]);

        $query->andFilterWhere(['like', 'LOGIN_CADASTRO', $this->LOGIN_CADASTRO])
            ->andFilterWhere(['like', 'TITULO', $this->TITULO])
            ->andFilterWhere(['like', 'DT_APROVACAO', $this->DT_APROVACAO])
            ->andFilterWhere(['like', 'LOGIN_APROVACAO', $this->LOGIN_APROVACAO])
            ->andFilterWhere(['like', 'DT_INICIO', $this->DT_INICIO])
            ->andFilterWhere(['like', 'DT_TERMINO', $this->DT_TERMINO])
            ->andFilterWhere(['like', 'PERIODO', $this->PERIODO]);

        return $dataProvider;
    }
}
