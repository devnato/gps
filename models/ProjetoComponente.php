<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "PROJETO_COMPONENTE".
 *
 * @property double $ID_PROJETO_COMPONENTE
 * @property double $PROJETO_ID_PROJETO
 * @property double $COMPONENTE_ID_COMPONENTE
 * @property string $DESCRICAO
 * @property string $TITULO
 *
 * @property COMPONENTE $1
 * @property PROJETO $10
 */
class ProjetoComponente extends \yii\db\ActiveRecord
{    
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'PROJETO_COMPONENTE';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['PROJETO_ID_PROJETO', 'COMPONENTE_ID_COMPONENTE'], 'required'],
            [['PROJETO_ID_PROJETO', 'COMPONENTE_ID_COMPONENTE'], 'number'],
            [['DESCRICAO'], 'string', 'max' => 2048],
            [['TITULO'], 'string', 'max' => 100],
            [['CODIGO'], 'string', 'max' => 20],
            [['1'], 'exist', 'skipOnError' => true, 'targetClass' => COMPONENTE::className(), 'targetAttribute' => ['1' => 'ID_COMPONENTE']],
            [['1'], 'exist', 'skipOnError' => true, 'targetClass' => PROJETO::className(), 'targetAttribute' => ['1' => 'ID_PROJETO']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'ID_PROJETO_COMPONENTE' => 'Id  Projeto  Componente',
            'PROJETO_ID_PROJETO' => 'Projeto  Id  Projeto',
            'COMPONENTE_ID_COMPONENTE' => 'Componente  Id  Componente',
            'DESCRICAO' => 'Descricao',
            'TITULO' => 'Titulo',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function get1()
    {
        return $this->hasOne(COMPONENTE::className(), ['ID_COMPONENTE' => '1']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function get10()
    {
        return $this->hasOne(PROJETO::className(), ['ID_PROJETO' => '1']);
    }
}
