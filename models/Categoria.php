<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "CATEGORIA".
 *
 * @property string $ID_CATEGORIA
 * @property string $DC_TIPO_GASTO
 */
class Categoria extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'CATEGORIA';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [            
            [['ID_CATEGORIA'], 'number'],
            [['DC_TIPO_GASTO'], 'string', 'max' => 100],
            [['ID_CATEGORIA'], 'unique'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'ID_CATEGORIA' => 'Id  Categoria',
            'DC_TIPO_GASTO' => 'Dc  Tipo  Gasto',
        ];
    }
}
