<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "MODELO_COMPONENTE".
 *
 * @property double $ID_COMPONENTE
 * @property double $ID_MODELO
 * @property double $ID_MODELO_COMPONENTE
 * @property double $CODIGO
 * @property double $ID_MODELO_COMPONENTE_PARENT
 *
 * @property COMPONENTE $1
 * @property MODELO $10
 * @property ModeloComponente $11
 * @property ModeloComponente[] $modeloComponentes
 */
class ModeloComponente extends \yii\db\ActiveRecord
{
    var $TITULO;
    
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'MODELO_COMPONENTE';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['ID_COMPONENTE', 'ID_MODELO'], 'required'],
            [['ID_COMPONENTE', 'ID_MODELO', 'ID_MODELO_COMPONENTE', 'ID_MODELO_COMPONENTE_PARENT'], 'number'],
            [['CODIGO'], 'string', 'max' => 20],          
            [['1'], 'exist', 'skipOnError' => true, 'targetClass' => COMPONENTE::className(), 'targetAttribute' => ['1' => 'ID_COMPONENTE']],
            [['1'], 'exist', 'skipOnError' => true, 'targetClass' => MODELO::className(), 'targetAttribute' => ['1' => 'ID_MODELO']],
            [['1'], 'exist', 'skipOnError' => true, 'targetClass' => ModeloComponente::className(), 'targetAttribute' => ['1' => 'ID_MODELO_COMPONENTE']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'ID_COMPONENTE' => 'Id  Componente',
            'ID_MODELO' => 'Id  Modelo',
            'ID_MODELO_COMPONENTE' => 'Id  Modelo  Componente',
            'CODIGO' => 'Codigo',
            'ID_MODELO_COMPONENTE_PARENT' => 'Id  Modelo  Componente  Parent',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function get1()
    {
        return $this->hasOne(COMPONENTE::className(), ['ID_COMPONENTE' => '1']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function get10()
    {
        return $this->hasOne(MODELO::className(), ['ID_MODELO' => '1']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function get11()
    {
        return $this->hasOne(ModeloComponente::className(), ['ID_MODELO_COMPONENTE' => '1']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getModeloComponentes()
    {
        return $this->hasMany(ModeloComponente::className(), ['1' => 'ID_MODELO_COMPONENTE']);
    }
    
    public function getTitulo(){
    	$titulo = "";
        if($this->TITULO != null && $this->TITULO != ""){
            $titulo = $this->TITULO;
        }else{
            $titulo = Componente::findOne(['ID_COMPONENTE'=> $this->ID_COMPONENTE])->TITULO;
        }
        return $titulo;        
    }
    
}
