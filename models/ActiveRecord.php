<?php

namespace app\models;

use yii\db\Expression;

class ActiveRecord extends \yii\db\ActiveRecord{
    private $dateFormat = 'YYYY-MM-DD hh24:mi:ss';

   public function dates()
    {
        return ['DT_EVENT'];
    }
    public function beforeSave($insert)
    {        
        if(parent::beforeSave($insert)){
            foreach($this->dates() as $attribute){
                $this->$attribute = new Expression("to_date('" . $this->$attribute . "','{$this->dateFormat}')");
            }
            return true;
        }else{
            return false;
        }
    }

    public function afterSave($insert, $changedAttributes)
    {
        // restore dates
        foreach($this->dates() as $attribute){
            $this->$attribute = str_replace(array("to_date('", "','{$this->dateFormat}')"), '', $this->$attribute->expression);
        }
    }
}