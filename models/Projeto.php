<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "PROJETO".
 *
 * @property double $ID_PROJETO
 * @property double $ID_MODELO
 * @property double $DT_CADASTRO
 * @property string $LOGIN_CADASTRO
 * @property string $TITULO
 * @property integer $STATUS
 * @property string $DT_APROVACAO
 * @property string $LOGIN_APROVACAO
 * @property string $DT_INICIO
 * @property string $DT_TERMINO
 * @property string $PERIODO
 * @property string $RESPONSAVEL
 *
 * @property ORCAMENTO[] $oRCAMENTOs
 * @property MODELO $1
 * @property PROJETOATIVIDADE[] $pROJETOATIVIDADEs
 * @property PROJETOCOMPONENTE[] $pROJETOCOMPONENTEs
 */
class Projeto extends \yii\db\ActiveRecord
{    
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'PROJETO';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['ID_MODELO', 'TITULO'], 'required'],
            [['ID_PROJETO', 'ID_MODELO', 'DT_CADASTRO'], 'number'],
            [['STATUS'], 'integer'],
            [['DT_APROVACAO', 'RESPONSAVEL'], 'string'],
            [['LOGIN_CADASTRO', 'LOGIN_APROVACAO'], 'string', 'max' => 20],
            [['TITULO'], 'string', 'max' => 100],
            [['PERIODO'], 'string', 'max' => 7],
            [['DESCRICAO'], 'string', 'max' => 150],
            [['SUBTITULO'], 'string', 'max' => 50],
            [['RESPONSAVEL'], 'string', 'max' => 255],
            [['1'], 'exist', 'skipOnError' => true, 'targetClass' => MODELO::className(), 'targetAttribute' => ['1' => 'ID_MODELO']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'ID_PROJETO' => 'Código',
            'ID_MODELO' => 'Modelo',
            'DT_CADASTRO' => 'Data Cadastro',
            'LOGIN_CADASTRO' => 'Login Cadastro',
            'TITULO' => 'Título',
            'STATUS' => 'Status',
            'DT_APROVACAO' => 'Data Aprovacao',
            'LOGIN_APROVACAO' => 'Login  Aprovacao',
            'DT_INICIO' => 'Data de Inicio',
            'DT_TERMINO' => 'Data de Término',
            'PERIODO' => 'Periodo',
            'RESPONSAVEL' => 'Responsável',
            'DESCRICAO' => 'Descrição',
            'SUBTITULO' => 'Subtítulo',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getORCAMENTOs()
    {
        return $this->hasMany(ORCAMENTO::className(), ['1' => 'ID_PROJETO']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function get1()
    {
        return $this->hasOne(MODELO::className(), ['ID_MODELO' => '1']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPROJETOATIVIDADEs()
    {
        return $this->hasMany(PROJETOATIVIDADE::className(), ['1' => 'ID_PROJETO']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPROJETOCOMPONENTEs()
    {
        return $this->hasMany(PROJETOCOMPONENTE::className(), ['1' => 'ID_PROJETO']);
    }
    
    public function getComponentes() {
        return $this->hasMany(Componente::className(), ['ID_COMPONENTE' => 'COMPONENTE_ID_COMPONENTE'])
          ->viaTable('PROJETO_COMPONENTE', ['PROJETO_ID_PROJETO' => 'ID_PROJETO']);
    }
    
    public function getModelo(){    	
        $modelo = Modelo::findOne(['ID_MODELO'=> $this->ID_MODELO])->DC_MODELO;        
        return $modelo;        
    }
}
