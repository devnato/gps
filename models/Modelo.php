<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "MODELO".
 *
 * @property double $ID_MODELO
 * @property string $DC_MODELO
 *
 * @property MODELOCOMPONENTE[] $mODELOCOMPONENTEs
 * @property PROJETO[] $pROJETOs
 */
class Modelo extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'MODELO';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['DC_MODELO'], 'required', 'message'=>'Este campo precisa ser preenchido.'],
            [['DC_MODELO'], 'string', 'max' => 100],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'ID_MODELO' => 'Código',
            'DC_MODELO' => 'Descrição',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMODELOCOMPONENTEs()
    {
        return $this->hasMany(MODELOCOMPONENTE::className(), ['ID_MODELO' => 'ID_MODELO']);
    }
    
    public function getComponentes() {
        return $this->hasMany(Componente::className(), ['ID_COMPONENTE' => 'ID_COMPONENTE'])
          ->viaTable('MODELO_COMPONENTE', ['ID_MODELO' => 'ID_MODELO']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPROJETOs()
    {
        return $this->hasMany(PROJETO::className(), ['1' => 'MODELO_ID']);
    }
}
