<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "ORCAMENTO".
 *
 * @property string $ID_PROJETO
 * @property string $ID_GASTO
 * @property string $QUANTIDADE
 * @property string $VALOR_UNITARIO
 * @property string $VALOR_TOTAL
 * @property string $TIPO
 * @property string $ID_ORCAMENTO
 * @property string $ID_TIPO_GASTO
 */
class Orcamento extends \yii\db\ActiveRecord
{
    
    var $GASTO;
    var $CATEGORIA;
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'ORCAMENTO';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['ID_PROJETO', 'ID_GASTO', 'ID_TIPO_GASTO'], 'required'],
            [['ID_PROJETO', 'ID_GASTO', 'QUANTIDADE', 'ID_ORCAMENTO', 'ID_TIPO_GASTO'], 'number'],
            [['TIPO'], 'string', 'max' => 1],
            [['ID_ORCAMENTO'], 'unique'],
            [['VALOR_UNITARIO', 'VALOR_TOTAL', 'QUANTIDADE'], 'safe'],    
            [['ID_PROJETO'], 'exist', 'skipOnError' => true, 'targetClass' => PROJETO::className(), 'targetAttribute' => ['ID_PROJETO' => 'ID_PROJETO']],
            [['1'], 'exist', 'skipOnError' => true, 'targetClass' => Gasto::className(), 'targetAttribute' => ['1' => 'ID_GASTO']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'ID_PROJETO' => 'Id  Projeto',
            'ID_GASTO' => 'Id  Gasto',
            'QUANTIDADE' => 'Quantidade',
            'VALOR_UNITARIO' => 'Valor  Unitário',
            'VALOR_TOTAL' => 'Valor  Total',
            'TIPO' => 'Tipo',
            'ID_ORCAMENTO' => 'Id  Orcamento',
            'ID_TIPO_GASTO' => 'Id  Tipo  Gasto',
        ];
    }
    
    /**
     * @return \yii\db\ActiveQuery
     */
    public function get10()
    {
        return $this->hasOne(Gasto::className(), ['ID_GASTO' => '1']);
    }
    
    public function getGASTO(){
        return Gasto::findOne(['ID_GASTO' => $this->ID_GASTO])->DESCRICAO;
    }
    
    public function getCATEGORIA(){
        return Categoria::findOne(['ID_CATEGORIA' => $this->ID_TIPO_GASTO])->DC_TIPO_GASTO;
    }
    
    public function getVALOR_UNITARIO_REAL(){
        return "R$ ".number_format($this->VALOR_UNITARIO, 2, ',', '.');
    }
    
    public function getVALOR_TOTAL_REAL(){
        return "R$ ".number_format($this->VALOR_TOTAL, 2, ',', '.');
    }
}
