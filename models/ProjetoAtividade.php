<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "PROJETO_ATIVIDADE".
 *
 * @property double $ID_PROJETO_ATIVIDADE
 * @property string $DT_INICIO
 * @property string $DT_FIM
 * @property double $ID_ATIVIDADE
 * @property double $ID_PROJETO
 * @property string $STATUS
 * @property double $RESPONSAVEL
 *
 * @property ATIVIDADERESPONSAVEL[] $aTIVIDADERESPONSAVELs
 * @property ATUALIZACAOATIVIDADE[] $aTUALIZACAOATIVIDADEs
 * @property ATIVIDADE $1
 * @property PROJETO $10
 */
class ProjetoAtividade extends \yii\db\ActiveRecord
{
    var $TITULO;
    var $ATIVIDADE;
    var $RESPONSAVELLABEL;
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'PROJETO_ATIVIDADE';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [                       
            [['ID_ATIVIDADE', 'ID_PROJETO', 'RESPONSAVEL'], 'number'],
            [['STATUS'], 'string', 'max' => 45],    
            [['DT_FIM', 'DT_INICIO'], 'safe'],            
            [['1'], 'exist', 'skipOnError' => true, 'targetClass' => ATIVIDADE::className(), 'targetAttribute' => ['1' => 'ID_ATIVIDADE']],
            [['1'], 'exist', 'skipOnError' => true, 'targetClass' => PROJETO::className(), 'targetAttribute' => ['1' => 'ID_PROJETO']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'ID_PROJETO_ATIVIDADE' => 'Id  Projeto  Atividade',
            'DT_INICIO' => 'Data  Inicio',
            'DT_FIM' => 'Data  Fim',
            'ID_ATIVIDADE' => 'Atividade',
            'ID_PROJETO' => 'Projeto',
            'STATUS' => 'Status',
            'RESPONSAVEL' => 'Responsavel',
            'RESPONSAVELLABEL' => 'Responsável',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getATIVIDADERESPONSAVELs()
    {
        return $this->hasMany(ATIVIDADERESPONSAVEL::className(), ['1' => 'ID_PROJETO_ATIVIDADE']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getATUALIZACAOATIVIDADEs()
    {
        return $this->hasMany(ATUALIZACAOATIVIDADE::className(), ['1' => 'ID_PROJETO_ATIVIDADE']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function get1()
    {
        return $this->hasOne(ATIVIDADE::className(), ['ID_ATIVIDADE' => '1']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function get10()
    {
        return $this->hasOne(PROJETO::className(), ['ID_PROJETO' => '1']);
    }
    
    public function getDescricaoAtividade(){
        return Atividade::findOne(['ID_ATIVIDADE'=> $this->ID_ATIVIDADE])->DC_ATIVIDADE;
    }

    public function getDTINICIO(){
        return Yii::$app->formatter->asDate($this->DT_INICIO, 'php:d/m/Y');
    }
    
    public function getDTFIM(){
        return Yii::$app->formatter->asDate($this->DT_FIM, 'php:d/m/Y');
    }
    
    public function getRESPONSAVEL(){
        $connection = Yii::$app->getDb("db2");
        $command = $connection->createCommand("SELECT "                    
                    . "                                     NM_USUARIO "
                    . "                                 FROM "
                    . "                                     BD_SICA.USUARIOS "
                    . "                                 WHERE "
                    . "                                     CD_USUARIO = ".$this->RESPONSAVEL."");                        
        $result = $command->queryAll();                              
        return ($result)?$result[0]['NM_USUARIO']:"";
    }
    
    public function getATIVIDADE(){
        return Atividade::findOne(['ID_ATIVIDADE' => $this->ID_ATIVIDADE])->DC_ATIVIDADE;
    }
}
