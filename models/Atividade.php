<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "ATIVIDADE".
 *
 * @property double $ID_ATIVIDADE
 * @property string $DC_ATIVIDADE
 *
 * @property PROJETOATIVIDADE[] $pROJETOATIVIDADEs
 */
class Atividade extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'ATIVIDADE';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [            
            [['ID_ATIVIDADE'], 'number'],
            [['DC_ATIVIDADE'], 'string', 'max' => 100],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'ID_ATIVIDADE' => 'Id  Atividade',
            'DC_ATIVIDADE' => 'Dc  Atividade',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPROJETOATIVIDADEs()
    {
        return $this->hasMany(PROJETOATIVIDADE::className(), ['1' => 'ID_ATIVIDADE']);
    }    
}
