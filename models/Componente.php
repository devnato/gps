<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "COMPONENTE".
 *
 * @property double $ID_COMPONENTE
 * @property string $TITULO
 * @property integer $TIPO
 *
 * @property MODELOCOMPONENTE[] $mODELOCOMPONENTEs
 * @property PROJETOCOMPONENTE[] $pROJETOCOMPONENTEs
 */
class Componente extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'COMPONENTE';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['TITULO'], 'required', 'message'=>'Informe um tículo.'],
            [['TIPO'], 'integer'],
            [['TITULO'], 'string', 'max' => 100],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'ID_COMPONENTE' => 'Id  Componente',
            'TITULO' => 'Titulo',
            'TIPO' => 'Tipo',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMODELOCOMPONENTEs()
    {
        return $this->hasMany(MODELOCOMPONENTE::className(), ['1' => 'ID_COMPONENTE']);
    }
    
    public function getModelos() {
        return $this->hasMany(Componente::className(), ['ID_MODELO' => 'ID_MODELO'])
          ->viaTable('MODELO_COMPONENTE', ['ID_COMPONENTE' => 'ID_COMPONENTE']);
    }
    
    public function getProjetos() {
        return $this->hasMany(Componente::className(), ['PROJETO_ID_PROJETO' => 'PROJETO_ID_PROJETO'])
          ->viaTable('MODELO_COMPONENTE', ['COMPONENTE_ID_COMPONENTE' => 'ID_COMPONENTE']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPROJETOCOMPONENTEs()
    {
        return $this->hasMany(PROJETOCOMPONENTE::className(), ['1' => 'ID_COMPONENTE']);
    }
}
