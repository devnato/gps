<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "GASTO".
 *
 * @property string $ID_GASTO
 * @property string $DESCRICAO
 */
class Gasto extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'GASTO';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [            
            [['ID_GASTO'], 'number'],
            [['DESCRICAO'], 'string', 'max' => 100],
            [['ID_GASTO'], 'unique'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'ID_GASTO' => 'Id  Gasto',
            'DESCRICAO' => 'Descricao',
        ];
    }
}
