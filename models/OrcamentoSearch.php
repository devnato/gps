<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Orcamento;

/**
 * OrcamentoSearch represents the model behind the search form about `app\models\Orcamento`.
 */
class OrcamentoSearch extends Orcamento
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['ID_PROJETO', 'ID_GASTO', 'QUANTIDADE', 'VALOR_UNITARIO', 'VALOR_TOTAL', 'ID_ORCAMENTO'], 'number'],
            [['TIPO'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Orcamento::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'ID_PROJETO' => $this->ID_PROJETO,
            'ID_GASTO' => $this->ID_GASTO,
            'QUANTIDADE' => $this->QUANTIDADE,
            'VALOR_UNITARIO' => $this->VALOR_UNITARIO,
            'VALOR_TOTAL' => $this->VALOR_TOTAL,
            'ID_ORCAMENTO' => $this->ID_ORCAMENTO,
        ]);

        $query->andFilterWhere(['like', 'TIPO', $this->TIPO]);

        return $dataProvider;
    }
}
