<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\ProjetoAtividade;

/**
 * ProjetoAtividadeSearch represents the model behind the search form about `app\models\ProjetoAtividade`.
 */
class ProjetoAtividadeSearch extends ProjetoAtividade
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['ID_PROJETO_ATIVIDADE', 'ID_ATIVIDADE', 'ID_PROJETO'], 'number'],
            [['DT_INICIO', 'DT_FIM', 'STATUS'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = ProjetoAtividade::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'ID_PROJETO_ATIVIDADE' => $this->ID_PROJETO_ATIVIDADE,
            'ID_ATIVIDADE' => $this->ID_ATIVIDADE,
            'ID_PROJETO' => $this->ID_PROJETO,
        ]);

        $query->andFilterWhere(['like', 'DT_INICIO', $this->DT_INICIO])
            ->andFilterWhere(['like', 'DT_FIM', $this->DT_FIM])
            ->andFilterWhere(['like', 'STATUS', $this->STATUS]);

        return $dataProvider;
    }
}
